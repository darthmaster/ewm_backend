# ElnaWebManager backend API

## Описание
- Backend API сервер для веб-интерфейса БС
- По-умолчанию имеет пользователя `admin` c паролем `1`
- Swagger UI: `https://localhost:8000/docs` 

## Требования
- Python ^3.9
- Poetry ^1.4.0
- Модули python: `lxml`, `fastapi`, `ping3`, `psutil`, `python-jose`, `passlib[bcrypt]`, `sqlalchemy`, `requests` (рекомендуется устновка с помощью `Poetry`)
- Пакет `python3-modules-sqlite3` (для AltLinux)
- Пакет `openssl`
- Библиотека `libelpk4.64.so` в `/usr/lib64/elna/`

## Автоматическая установка (ALT Linux)
```
curl -fsSL https://gitlab.com/darthmaster/ewm_back/-/raw/main/install.sh | sudo bash
```

## Ручная устнаовка и запуск
1. Для предотвращения возможных ошибок доступа при проверке соединения, добавить в файл `/etc/rc.d/rc.local` команду:
    ```
    sysctl net.ipv4.ping_group_range='0   2147483647'
    ```
    > В случае ошибки `sysctl: setting key "net.ipv4.ping_group_range": Invalid argument`
    > использовать ровно три пробела между числами в значении параметра
2. Установить зависимости (указаны имена пакетов для ALT Linux):
```
sudo apt-get install git openssl python3-module-poetry python3-modules-sqlite3
```
3. Сгенерировать ключ шифрования и сертификат безопасности:
```
openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /opt/Elna/ewm.key -out /opt/Elna/ewm.crt
```
4. Клонировать репозиторий:
```
git clone https://gitlab.com/darthmaster/ewm_back.git
```
<!-- или
```
git clone http://192.168.0.198:3000/albert/ewm_back
``` -->
5. Перейти в папку с исходным кодом:
```
cd ewm_back/src
```
6. Установить зависимости python:
```
sudo poetry install
```
7. Для запуска использовать команду:
```
sudo poetry run python main.py
```

## Запуск как сервис systemd
1. Создать файл `/lib/systemd/system/ewm_backend.service`:
```
[Unit]
Description=ElnaWebManager backend API

[Service]
Type=simple
WorkingDirectory=/opt/Elna/ewm_back/src
ExecStart=poetry run python main.py
Restart=always
User=root

[Install]
WantedBy=multi-user.target
```
2. Перезагрузить systemd:
```
sudo systemctl daemon-reload
```
3. Включить сервис:
```
sudo systemctl enable --now ewm_backend.service
```

### Просмотр лога
```
journalctl -xefu ewm_backend
```

## Обновление
1. Перейти в папку локального репозитория:
```
cd /opt/Elna/ewm_back
```
2. Загрузить обновление из удалённого репозитория:
```
sudo git pull
```
3. Актуализировать зависимости:
```
sudo poetry install
```
4. Перезапустить сервис:
```
sudo systemctl restart ewm_backend.service
```