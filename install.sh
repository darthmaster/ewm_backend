#!/bin/bash

apt-get update
apt-get install -y terminfo terminfo-extra git python3-module-poetry python3-modules-sqlite3 openssl
cd /opt/Elna
git clone https://gitlab.com/darthmaster/ewm_back.git
cd ewm_back
poetry install
cp ewmb.service /lib/systemd/system/ewm_backend.service
systemctl daemon-reload
systemctl enable --now ewm_backend.service
systemctl restart nginx.service