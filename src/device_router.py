import fcntl
import ipaddress
import os
import re
import socket
import struct
import subprocess
import asyncio

import psutil
from schemas import DeviceSetupParams, DeviceSetupResult
from dependencies import EWM_Context, get_current_user, get_current_user_ws
from fastapi import APIRouter, WebSocket, Depends, HTTPException
from fastapi.responses import FileResponse
from schemas import User, Section
from WebSocketChannel import WebSocketChannel

device_router = APIRouter(prefix="/device")
context = EWM_Context()


def get_hostname():
    return socket.gethostname()


def set_hostname(new_hostname: str):
    try:
        with open("/etc/hostname", "w") as hostname_file:
            hostname_file.write(new_hostname)
        subprocess.run(
            ["hostname", "set-hostname", new_hostname], check=True, capture_output=True
        )
        context.log.warning(f"Имя машины изменено ({new_hostname})")
        return True, "Имя машины изменено"
    except subprocess.CalledProcessError as e:
        err = e.stderr.decode().strip()
        context.log.error(f"Ошибка во время изменения имени машины: {err}")
        return False, err


def get_ip_info(ifname):
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:
        ip_address = socket.inet_ntoa(
            fcntl.ioctl(
                s.fileno(),
                0x8915,  # SIOCGIFADDR
                struct.pack("256s", bytes(ifname[:15], "utf-8")),
            )[20:24]
        )

        netmask = socket.inet_ntoa(
            fcntl.ioctl(
                s.fileno(),
                0x891B,  # SIOCGIFNETMASK
                struct.pack("256s", bytes(ifname[:15], "utf-8")),
            )[20:24]
        )

        # Преобразование маски подсети в CIDR нотацию
        netmask_cidr = ipaddress.IPv4Network(f"0.0.0.0/{netmask}").prefixlen

        return f"{ip_address}/{netmask_cidr}"
    except OSError as e:
        if e.errno == 19:
            return f"Интерфейс {ifname} не найден"
        else:
            return e.strerror


def set_ip(interface: str, cidr: str):
    try:
        # _ip =ipaddress.ip_address(ip_address)
        # if not (0 <= subnet_mask <= 32):
        #     return False
        # cidr = f"{ip_address}/{subnet_mask}"
        subprocess.run(
            ["ip", "addr", "flush", "dev", interface], check=True, capture_output=True
        )
        subprocess.run(
            ["ip", "addr", "add", cidr, "dev", interface],
            check=True,
            capture_output=True,
        )
        context.log.warning(
            f"Для интерфейса {interface} установлен новый адрес: {cidr}"
        )
        return True, "Установлен новый адрес"
    except subprocess.CalledProcessError as e:
        err = e.stderr.decode().strip()
        context.log.error(
            f"Ошибка при установке IP-адреса для интерфейса {interface}: {err}"
        )
        return False, err


def set_service_status(service, status):
    cmd = ["systemctl", "enable" if status else "disable", service]
    result = subprocess.run(cmd, capture_output=True)
    return not bool(result.returncode), result.stderr.decode().strip()


def is_service_active(service):
    cmd = f"systemctl is-enabled {service}"
    status = (
        subprocess.run([cmd], shell=True, capture_output=True).stdout.strip().decode()
    )
    return status == "enabled"


def get_timesync_info(config_file_path="/etc/chrony.conf"):
    service_enabled = is_service_active("chronyd")
    server_address = None
    subnet_address = None
    server_mode = None
    try:
        with open(config_file_path) as file:
            for line in file:
                try:
                    if line.startswith("server") and not server_address:
                        server_address = line.split()[1]
                    if (
                        line.strip().startswith(("allow", "# allow"))
                        and not subnet_address
                    ):
                        subnet_address = line.split()[1]
                        server_mode = line[0] != "#"
                except IndexError:
                    context.log.info(f"Некорректная строка в {config_file_path}")
                    continue
        return service_enabled, server_mode, server_address, subnet_address
    except FileNotFoundError:
        return service_enabled, server_mode, server_address, subnet_address


def edit_chrony_conf(server_address, server_mode, subnet_address) -> tuple[bool, str]:
    try:
        with open("/etc/chrony.conf", "r") as conf_file:
            lines = conf_file.readlines()

        for i, line in enumerate(lines):
            if line.startswith("server"):
                parts = line.split()
                parts[1] = server_address
                lines[i] = " ".join(parts) + "\n"
                break

        for i, line in enumerate(lines):
            if line.strip().startswith("allow") and not server_mode:
                lines[i] = f"# {line}"
            elif line.strip().startswith("# allow") and server_mode:
                lines[i] = line.lstrip("#")

        for i, line in enumerate(lines):
            if line.strip().startswith("allow"):
                parts = line.split()
                if len(parts) >= 2:
                    parts[1] = subnet_address
                    lines[i] = " ".join(parts) + "\n"
                    break

        with open("/etc/chrony.conf", "w") as conf_file:
            conf_file.writelines(lines)

        context.log.warning("chrony.conf изменён")
        return True, "chrony.conf изменён"
    except Exception as e:
        context.log.error(f"Ошибка при изменении chrony.conf: {e}")
        return False, str(e)


def get_spo_version():
    try:
        with open("/home/user/elna.base/sw_list.txt", encoding="utf-8") as file:
            line = file.readline()
            return line[line.find(":") + 1 : line.find("Дата")].strip()
    except FileNotFoundError:
        return "номер версии не найден"


def get_elplc_version():
    try:
        files = os.listdir("/opt/ineum/runtime/")
    except FileNotFoundError:
        return "не установлен"

    version_files = [f for f in files if re.match(r"elplc-runtime-\d+\.\d+\.\d+", f)]
    version_files.sort(key=lambda f: [int(num) for num in re.findall(r"\d+", f)])
    latest_file = version_files[-1] if version_files else None

    if latest_file:
        version_number = re.search(r"elplc-runtime-(\d+\.\d+\.\d+)", latest_file)
        if version_number:
            return version_number.group(1)

    return "номер версии не найден"


@device_router.get("/hwstat")
async def get_hwstat():
    # Загруженность процессора
    cpu_usage = psutil.cpu_percent(interval=1, percpu=True)

    # Температура процессора
    cpu_temp = None
    sensors = psutil.sensors_temperatures()
    if sensors:
        try:
            current_temp = sensors["coretemp"][0].current
            cpu_temp = f"{current_temp}°C"
        except KeyError:
            cpu_temp = "температурные данные не найдены"

    # Свободное место на жёстком диске
    hdd = psutil.disk_usage("/")
    free_hdd = hdd.free / (2**30)  # Преобразование в гигабайты

    # Свободная оперативная память
    ram = psutil.virtual_memory()
    free_ram = ram.available / (2**30)  # Преобразование в гигабайты

    # Время работы системы
    uptime = os.popen("uptime -p").read().strip()

    return {
        "cpu_usage": cpu_usage,
        "cpu_temp": cpu_temp,
        "free_hdd_gb": f"{free_hdd:.2f} GB",
        "free_ram_gb": f"{free_ram:.2f} GB",
        "uptime": uptime,
    }


hwstat_channel = WebSocketChannel(get_hwstat, 1)


@device_router.websocket("/hwstat")
async def hwstat_websocket_endpoint(
    ws: WebSocket, user: User = Depends(get_current_user_ws)
):
    await hwstat_channel.handle_connection(ws)


@device_router.get("/date")
async def get_date():
    cmd = "date +'%Y-%m-%d %k:%M:%S %Z'"
    return (
        subprocess.run([cmd], shell=True, capture_output=True)
        .stdout.strip()
        .decode("utf-8")
    )


date_channel = WebSocketChannel(get_date, 1)


@device_router.websocket("/date")
async def date_websocket_endpoint(
    ws: WebSocket, user: User = Depends(get_current_user_ws)
):
    await date_channel.handle_connection(ws)


async def get_device_summary():
    hwstat = await get_hwstat()
    date = await get_date()
    return {"hwstat": hwstat, "date": date}


device_channel = WebSocketChannel(get_device_summary, 1)


@device_router.websocket("")
async def device_ws_endpoint(ws: WebSocket, user: User = Depends(get_current_user_ws)):
    await device_channel.handle_connection(ws)


def is_service_active(service_name) -> bool:
    cmd = ["systemctl", "is-active", service_name]
    is_service_active = (
        subprocess.run(cmd, capture_output=True, text=True).stdout == "active\n"
    )
    return is_service_active


@device_router.get("/services")
async def get_device_services(user: User = Depends(get_current_user)):
    result = {}
    services_list = ["elnaparam", "elnaiousb", "elplc-runtime"]
    if context.section_role == "slave":
        services_list.append("elnaclient")
    elif context.section_role == "master":
        for i, section in enumerate(context.controller_sections):
            if section != Section():
                services_list.append(f"elnaserver{i+1}")
    for service in services_list:
        result.update({service: is_service_active(service)})
    return result


def set_date(date):
    try:
        subprocess.run(["timedatectl", "set-time", date], check=True)
        context.log.warning(f"Установлена новая дата {date}")
        return True, "Установлена новая дата"
    except subprocess.CalledProcessError as e:
        err = e.stderr.decode().strip()
        context.log.error(f"Ошибка при установке даты: {err}")
        return False, err


@device_router.get("/info")
async def get_device_info(user: User = Depends(get_current_user)):
    return {
        "serial": context.device_serial,
        "hostname": get_hostname(),
        "lan1": get_ip_info(ifname=context.iface_lan1),
        "lan2": get_ip_info(ifname=context.iface_lan2),
        "date": await get_date(),
        "hw_stat": await get_hwstat(),
        "timesync": dict(
            zip(
                ("service_enabled", "server_mode", "server_address", "subnet_address"),
                get_timesync_info(),
            )
        ),
        "spo_version": get_spo_version(),
        "elplc_version": get_elplc_version(),
    }


@device_router.post("/setup", response_model=DeviceSetupResult)
async def set_device_params(
    params: DeviceSetupParams, user: User = Depends(get_current_user)
):
    results = DeviceSetupResult()
    try:
        if params.timesync:
            results.set_chronyd_service_status = set_service_status(
                "chronyd", params.timesync.service_enabled
            )
            results.edit_chrony_conf = edit_chrony_conf(
                params.timesync.server_address,
                params.timesync.server_mode,
                params.timesync.subnet_address,
            )
        if params.date:
            results.set_date = set_date(params.date)
        if params.hostname:
            results.set_hostname = set_hostname(params.hostname)
        if params.lan1:
            results.set_lan1_ip = set_ip(interface=context.iface_lan1, cidr=params.lan1)
        if params.lan2:
            results.set_lan2_ip = set_ip(interface=context.iface_lan2, cidr=params.lan2)
    except Exception as e:
        context.log.error(f"Ошибка при изменении настроек машины: {e}")
        results.set_device_params = False, str(e)
    return results


def get_elplc_files(log_dir="/var/log"):
    return [
        f
        for f in os.listdir(log_dir)
        if f.startswith("elplc") and os.path.isfile(os.path.join(log_dir, f))
    ]


@device_router.get("/log/files")
async def get_log_files(user: User = Depends(get_current_user)):
    try:
        file_list = []
        elplc_files = get_elplc_files("/var/log")
        file_list.extend(elplc_files)

        for root, dirs, files in os.walk("/var/log/elna/"):
            for file in files:
                file_list.append(file)
        return file_list
    except Exception as e:
        context.log.error(f"Ошибка при получении списка логов: {e}")
        return [{"error": str(e)}]


@device_router.get("/log/file")
async def get_log_file_content(filename, user: User = Depends(get_current_user)):
    try:
        if filename.startswith("elplc"):
            file_path = f"/var/log/{filename}"
        else:
            file_path = f"/var/log/elna/{filename}"

        with open(file_path, "r") as f:
            return f.read()
    except Exception as e:
        context.log.error(f"Ошибка при получении содержимого лога {filename}: {e}")
        return {"error": str(e)}


@device_router.get("/log/file/download")
async def download_log_file(filename, user: User = Depends(get_current_user)):
    try:
        if filename.startswith("elplc"):
            file_path = f"/var/log/{filename}"
        else:
            file_path = f"/var/log/elna/{filename}"

        return FileResponse(path=file_path, filename=filename)
    except Exception as e:
        context.log.error(f"Ошибка при скачивании лога {filename}: {e}")
        return {"error": str(e)}


@device_router.get("/log/services")
async def get_services(user: User = Depends(get_current_user)):
    services = [f"elnaserver{i}" for i in range(1, 8)]
    services.append("elnaclient")
    enabled_services = []
    for service in services:
        if is_service_active(service):
            enabled_services.append(service)
    return enabled_services


@device_router.get("/log/service")
async def get_service_journal(
    service_name, num_lines=100, user: User = Depends(get_current_user)
):
    try:
        result = subprocess.run(
            ["journalctl", f"--unit={service_name}", f"--lines={num_lines}"],
            capture_output=True,
            text=True,
        )
        return result.stdout
    except Exception as e:
        context.log.error(f"Ошибка при получении журнала сервиса {service_name}: {e}")
        return {"error": str(e)}


@device_router.get("/log/service/download")
async def download_service_journal(
    service_name, num_lines=10000, user: User = Depends(get_current_user)
):
    try:
        result = subprocess.run(
            ["journalctl", f"--unit={service_name}", f"--lines={num_lines}"],
            capture_output=True,
            text=True,
        )
        with open(f".{service_name}.log", "w") as f:
            f.write(result.stdout)
        return FileResponse(path=f".{service_name}.log", filename=f"{service_name}.log")
    except Exception as e:
        context.log.error(f"Ошибка при скачивании журнала сервиса {service_name}: {e}")
        return {"error": str(e)}


@device_router.get("/log/dump")
async def get_logs_dump():
    try:
        result = subprocess.run(
            ["./log_dump.sh"], stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True
        )
        archive_path = result.stdout.strip()

        if os.path.exists(archive_path):
            return FileResponse(
                path=archive_path,
                filename=os.path.basename(archive_path),
                media_type="application/x-tar",
            )
        else:
            raise HTTPException(status_code=404, detail="Archive not found")

    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))
