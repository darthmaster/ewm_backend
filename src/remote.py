from urllib.parse import urljoin
import requests
import base64
import os

from schemas import Section


def check_health(base_url):
    """Проверка доступности бекенда."""
    try:
        resp = requests.get(f"{base_url}/health", timeout=2, verify=False)
        return resp.status_code == 200
    except requests.RequestException:
        return False


def get_remote_serial(base_url):
    """Получение серийного номера с удалённого бекенда."""
    resp = requests.get(f"{base_url}/serial", timeout=5, verify=False)
    if resp.status_code == 200:
        data = resp.json()
        return data.get("serial")
    return None


def get_remote_role(base_url):
    """Получение роли с удалённого бекенда."""
    resp = requests.get(f"{base_url}/role", timeout=5, verify=False)
    if resp.status_code == 200:
        data = resp.json()
        return data.get("role")
    return None


def set_remote_master(base_url, section: Section):
    resp = requests.put(
        f"{base_url}/master", json=section.dict(), timeout=5, verify=False
    )
    if resp.status_code == 200:
        return True
    else:
        return False


def download_file(base_url, remote_path, local_dir="./downloaded", chunk_size=8192):
    """Загрузка файла с удаленного устройства."""
    url = f"{base_url}/file"

    try:
        resp = requests.post(url, json={"path": remote_path}, timeout=10, verify=False)
        resp.raise_for_status()

        if not os.path.exists(local_dir):
            os.makedirs(local_dir)

        filename = os.path.basename(remote_path)
        local_path = os.path.join(local_dir, filename)

        with open(local_path, "wb") as f:
            f.write(resp.content)

        print(f"Файл {remote_path} загружен в {local_path}")
        return True

    except requests.exceptions.RequestException as e:
        print(f"Не удалось загрузить файл {remote_path}. Ошибка: {e}")
        return False

    except IOError as e:
        print(f"Ошибка при сохранении файла {local_path}. Ошибка: {e}")
        return False


def upload_file(base_url, local_path, remote_path):
    """Выгрузка файла на удаленный бекенд."""
    filename = os.path.basename(local_path)
    with open(local_path, "rb") as f:
        files = {"file": (filename, f)}
        data = {"remote_path": remote_path}
        resp = requests.post(
            f"{base_url}/file/upload", files=files, data=data, timeout=10, verify=False
        )
        if resp.status_code == 200:
            print(f"Файл {local_path} выгружен на {remote_path}")
            return True
        else:
            print(
                f"Не удалось выгрузить файл {local_path}. Код ответа: {resp.status_code}"
            )
            return False


def manage_remote_service(base_url, service_name, action):
    """Запуск или остановка сервиса на удаленном хосте."""
    payload = {"service_name": service_name, "action": action}
    resp = requests.post(
        f"{base_url}/service/manage", json=payload, timeout=5, verify=False
    )
    if resp.status_code == 200:
        print(f"Сервис {service_name} - команда '{action}' выполнена успешно.")
        return True
    else:
        print(
            f"Ошибка при выполнении команды для {service_name}: {resp.status_code}, {resp.text}"
        )
        return False
