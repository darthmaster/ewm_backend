import os
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

SQLALCHEMY_DATABASE_URL = "sqlite:///ewm.db"

engine = create_engine(
    SQLALCHEMY_DATABASE_URL, connect_args={"check_same_thread": False}
)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)
Base = declarative_base()


def init_db():
    if not os.path.exists("test.db"):
        Base.metadata.create_all(bind=engine)
        from crud import create_user, get_user
        from schemas import UserCreate

        db = SessionLocal()
        if not get_user(db, username="admin"):
            default_user = UserCreate(username="admin", password="1")
            create_user(db=db, user=default_user)
        db.close()


def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()
