import os
from copy import deepcopy
from datetime import datetime
from pathlib import Path
from typing import AnyStr, List

import lxml.etree as ET
from classes import BS, Module, Project


def generate_station_xml(bs: BS, root: ET.ElementBase):
    station = ET.SubElement(root, "station", type="own", lan1=bs.ip1, lan2=bs.ip2)  # type: ignore
    for index, module in enumerate(bs.modules):
        position = ET.SubElement(station, "position", number=str(index + 1))  # type: ignore
        type_tag = ET.SubElement(position, "type", attrib=module.get_type_tag_dict())  # type: ignore
        type_tag.text = str(module.int_type)
        params_copy = deepcopy(module.params)
        params_copy.pop("power", None)
        params_set = set(params_copy)
        param_names_set = {"breakage", "shortcut"}
        if param_names_set.intersection(params_set):
            ET.SubElement(position, "flags", module.params)  # type: ignore
        param_names_set = {"func", "guard", "break", "lost", "timeout", "dac"}
        if param_names_set.intersection(params_set):
            ET.SubElement(position, "control", module.params)  # type: ignore
    if any([bs.gpin_check, bs.gpout_check, bs.param_check, bs.power_check]):
        board = ET.SubElement(station, "board")  # type: ignore
        if bs.gpin_check:
            board.set("in", str(bs.gpin))
        if bs.gpout_check:
            board.set("out", str(bs.gpout))
        if bs.param_check:
            board.set("param", str(bs.param))
        if bs.power_check:
            board.set("power", str(bs.power))
    service = ET.SubElement(station, "service", life=str(bs.life))  # type: ignore
    if bs.cmd_check:
        service.set("cmd", str(bs.cmd))


def write_summary(bs_list: List[BS], pname: AnyStr):
    root = ET.Element("elpk-system")  # type: ignore
    current_datetime = datetime.today().strftime("%d.%m.%y %H:%M")
    comment = ET.Comment(f"Проект: {pname} ({current_datetime})")
    root.addprevious(comment)

    prv_bs = None
    for bs in bs_list:
        if bs:
            bs.calculate_indices(prv_bs.extreme_indices if prv_bs else None)
            prv_bs = bs
            generate_station_xml(bs, root)
    ET.ElementTree(root).write(
        "summary.xml", encoding="UTF-8", pretty_print=True, xml_declaration=True
    )


def write_xml(bs: BS, pname="", path="bsconfig.xml"):
    root = ET.Element("elpk-system")  # type: ignore
    current_datetime = datetime.today().strftime("%d.%m.%y %H:%M")
    comment = ET.Comment(f"Проект: {pname} ({current_datetime}) | БС-{bs.num}")
    root.addprevious(comment)
    generate_station_xml(bs, root)
    ET.ElementTree(root).write(
        path, encoding="UTF-8", pretty_print=True, xml_declaration=True
    )


def write_dat(bslist: List[BS], current_id):
    sn_values = {
        "AIN": 0,
        "DIN": 0,
        "AOUT": 0,
        "DOUT": 0,
        "AOIN": 0,
        "DOIN": 0,
        "ERR": 0,
        "SD": 0,
        "AIW": 0,
    }
    for i, bs in enumerate(bslist):
        if i == current_id or current_id == 0:
            break
        else:
            sn_values.update(
                (key, sn_values[key] + value)
                for key, value in bs.extreme_indices.items()
            )

    text = f"""IP_SERVER={bslist[0].ip1}
Port_Server=342{current_id}
\nsn_Ai_Dbl={sn_values["AIN"]}
n_Ai_Dbl={bslist[current_id].extreme_indices["AIN"]}
\nsn_Di={sn_values["DIN"]}
n_Di={bslist[current_id].extreme_indices["DIN"]}
\nsn_Err={sn_values["ERR"]}
n_Err={bslist[current_id].extreme_indices["ERR"]}
\nsn_CIRC={sn_values["DOIN"]}
n_CIRC={bslist[current_id].extreme_indices["DOIN"]}
\nsn_RelSt={sn_values["DOIN"]}
n_RelSt={bslist[current_id].extreme_indices["DOIN"]}
\nsn_AOIN={sn_values["AOIN"]}
n_AOIN={bslist[current_id].extreme_indices["AOIN"]}
\nsn_DOUT={sn_values["DOUT"]}
n_DOUT={bslist[current_id].extreme_indices["DOUT"]}
\nsn_AOUT={sn_values["AOUT"]}
n_AOUT={bslist[current_id].extreme_indices["AOUT"]}
\nsn_SD={sn_values["SD"]}
n_SD={bslist[current_id].extreme_indices["SD"]}
\nsn_AIW={sn_values["AIW"]}
n_AIW={bslist[current_id].extreme_indices["AIW"]}
\nsn_SCSC={current_id}
n_SCSC=1"""

    with open("ServerTCP.dat", "w") as f:
        f.write(text.strip())


def handle_station_tag(elem, bs, _unused):
    bs.ip1 = elem.get("lan1", "")
    bs.ip2 = elem.get("lan2", "")


def handle_position_tag(elem, _unused1, _unused2):
    return int(elem.get("number")) - 1


def handle_type_tag(elem, bs, pos):
    for key, value in elem.attrib.items():
        if key == "name":
            bs.modules[pos] = Module.from_modname(value)
        elif key == "power":
            bs.modules[pos].params[key] = value
        else:
            bs.modules[pos].initial_indices[key.upper()] = int(value)


def handle_params_tag(elem, bs, pos):
    bs.modules[pos].params = elem.attrib


def handle_service_tag(elem, bs, _unused):
    bs.life = elem.get("life")
    bs.cmd = elem.get("cmd", None)
    bs.cmd_check = bool(bs.cmd)


def handle_board_tag(elem, bs, _unused):
    bs.gpin = elem.get("in", None)
    bs.gpin_check = bool(bs.gpin)
    bs.gpout = elem.get("out", None)
    bs.gpout_check = bool(bs.gpout)
    bs.param = elem.get("param", None)
    bs.param_check = bool(bs.param)
    bs.power = elem.get("power", 0)
    bs.power_check = bool(bs.power)


def read_xml(bsnum: str, xmlfile="bsconfig.xml", fromstring=False):
    xml = ET.fromstring(xmlfile) if fromstring else ET.parse(xmlfile)  # type: ignore
    bs: BS = BS(num=int(bsnum))
    pos: int = -1

    tag_handlers = {
        "station": handle_station_tag,
        "position": handle_position_tag,
        "type": handle_type_tag,
        "flags": handle_params_tag,
        "control": handle_params_tag,
        "service": handle_service_tag,
        "board": handle_board_tag,
    }

    for elem in xml.iter():
        if elem.tag in tag_handlers:
            handler = tag_handlers[elem.tag]
            result = handler(elem, bs, pos)
            if elem.tag == "position":
                pos = result

    bs.calculate_indices()
    return f"БС-{bsnum}", bs


def load_project(project_dir):
    proj = Project()
    proj_dir = Path(project_dir)
    if proj_dir.exists():
        proj.name = proj_dir.name
        print(f"Загрузка проекта {proj.name}")
        proj_dir_content = list(proj_dir.iterdir())
        proj_dir_content.sort()
        print(proj_dir_content)
        for item in proj_dir_content:
            is_dir = item.is_dir()
            is_decimal = item.name.isdecimal()
            print(f"{item.name} => is_dir:{is_dir} is_decimal:{is_decimal}")
            if is_decimal:  # and is_dir
                _, bs = read_xml(item.name, item.joinpath("bsconfig.xml"))
                proj.stations.append(bs)
                print(f"БС-{item.name} загружен в проект")
        proj.stations.sort()
    return proj


def save_project(proj: Project, parent_dir="."):
    os.system(f"rm -rf {parent_dir}/{proj.name}")
    if parent_dir == ".":
        parent_dir = os.getcwd()
    if proj.stations:
        for index, bs in enumerate(proj.stations):
            if bs:
                path_string = f"{parent_dir}/{proj.name}/{bs.num}"
                Path(path_string).mkdir(exist_ok=True, parents=True)
                os.chdir(path_string)
                write_xml(bs, proj.name)
                write_dat(proj.stations, index)
                os.chdir("..")
    else:
        Path(f"{parent_dir}/{proj.name}/").mkdir(exist_ok=True, parents=True)
    write_summary(deepcopy(proj.stations), proj.name)
    os.chdir("..")
