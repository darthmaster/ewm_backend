import asyncio
import subprocess
from typing import Dict
from fastapi import APIRouter, Depends, WebSocket
from schemas import User, ModuleWriteData, SectionPowerChannelSetup
from classes import Module
from fileproc import write_xml, write_dat, read_xml
from dependencies import get_current_user, get_current_user_ws, EWM_Context
from WebSocketChannel import WebSocketChannel

section_router = APIRouter(prefix="/section")
context = EWM_Context()

ModuleState = {
    0: "ERRM_ZERO",  # Ноль, не должно быть
    1: "NO_LOCAL_MODULE",  # Нет модуля в локальном проекте
    2: "NO_REMOTE_MODULE",  # Нет модуля в удаленном проекте
    3: "BAN_EXCHANGE_LOCAL",  # Запрет обмена в локальном проекте
    4: "BAN_EXCHANGE_REMOTE",  # Запрет обмена в удаленном проекте
    5: "STOPED_LOCAL_THREAD",  # Остановлен локальный поток
    6: "STOPED_REMOTE_THREAD",  # Остановлен удаленный поток
    7: "LOCAL_THREAD_HUNG",  # Завис локальный поток
    8: "REMOTE_THREAD_HUNG",  # Завис удаленный поток
    9: "NOT_CONNECTED",  # Не подключен
    10: "LOST",  # Нет связи
    11: "WRONG_MODULE_TYPE",  # Другой тип модуля в проекте
    12: "ZERO_DATAIN",  # Нулевые входные данные
    13: "INIT_RUN",  # Инициализация
    14: "PORT_NOT_OPENED",  # Порт не открыт
    15: "NETWORK_PROG_ERROR",  # Ошибка сетевой программы
    16: "INVALID_PROG_VERSION",  # Странная версия прошивки модуля
    20: "SUCCESS",  # Все хорошо, OK
}

module_channels: Dict[int, WebSocketChannel] = {}


@section_router.get("")
async def get_section() -> Dict:
    modules_state = []
    for module in context.working_bsconfig.modules:
        status_code = module.read(context.elpk, "ERR", 0)["ERR"][0]["d_value"]
        modules_state.append(
            {
                "type": module.int_type,
                "status_code": status_code,
                "status_text": ModuleState.get(status_code, "UNKNOWN"),
            }
        )
    return {
        "gpin": context.working_bsconfig.gpin,
        "gpout": context.working_bsconfig.gpout,
        "cpu_params": context.working_bsconfig.param,
        "cmd_service": context.working_bsconfig.cmd,
        "power_channel": abs(int(context.working_bsconfig.power)),
        "power_channel_enabled": context.working_bsconfig.power_check,
        "power_channel_inverted": context.working_bsconfig.invert_power_channel,
        "modules": modules_state,
    }


section_channel: WebSocketChannel = WebSocketChannel(get_section, 1)


@section_router.websocket("")
async def section_websocket(ws: WebSocket, user: User = Depends(get_current_user_ws)):
    await section_channel.handle_connection(ws)


def apply_changes():
    write_xml(context.working_bsconfig, path=f"{context.path_iousb}/{context.xml_name}")
    subprocess.run(
        ["systemctl", "restart", "elnaiousb"], check=True, capture_output=True
    )


@section_router.get("/module/{pos}")
async def get_module(pos: int, user: User = Depends(get_current_user)):
    context.working_bsconfig.modules[pos].read(elpk=context.elpk)
    return {
        "name": context.working_bsconfig.modules[pos].str_type,
        "type": context.working_bsconfig.modules[pos].int_type,
        "params": context.working_bsconfig.modules[pos].params,
        "arrays_info": context.working_bsconfig.modules[pos].arrays_info,
        "arrays_data": context.working_bsconfig.modules[pos].data,
        "status_code": context.working_bsconfig.modules[pos].data["ERR"][0]["d_value"],
    }


async def read_module_data(pos):
    return context.working_bsconfig.modules[pos].read(elpk=context.elpk)


@section_router.websocket("/module/{pos}")
async def module_data_ws(
    pos: int, ws: WebSocket, user: User = Depends(get_current_user_ws)
):
    if pos not in module_channels.keys():
        module_channels[pos] = WebSocketChannel(lambda: read_module_data(pos), 0.2)
    await module_channels[pos].handle_connection(ws)


@section_router.post("/module/{pos}")
async def set_module(pos: int, type: int, user: User = Depends(get_current_user)):
    context.working_bsconfig.modules[pos] = Module.from_code(type)
    context.working_bsconfig.calculate_indices()
    return await get_module(pos)


@section_router.put("/module/{pos}")
async def set_module_params(
    pos: int, params: Dict[str, str], user: User = Depends(get_current_user)
):
    for key, value in params.items():
        context.working_bsconfig.modules[pos].params[key] = value.strip()
    apply_changes()
    await asyncio.sleep(2)
    return await get_section()


@section_router.post("/module/{pos}/data")
async def set_module_data(
    pos: int, data: ModuleWriteData, user: User = Depends(get_current_user)
):
    context.working_bsconfig.modules[pos].write(
        context.elpk, data.array, data.index, data.value, data.flag
    )


@section_router.get("/reset")
async def reload_bsconfig(user: User = Depends(get_current_user)):
    _, context.working_bsconfig = read_xml(
        "0", xmlfile=f"{context.path_iousb}/{context.xml_name}"
    )
    return await get_section()


@section_router.put("/power")
async def setup_power_channel(params: SectionPowerChannelSetup):
    context.working_bsconfig.power_check = params.enabled
    if params.enabled:
        context.working_bsconfig.invert_power_channel = params.inverted
        context.working_bsconfig.power = (
            -int(params.channel) if params.inverted else int(params.channel)
        )
    else:
        context.working_bsconfig.power = 0
        context.working_bsconfig.invert_power_channel = False
    apply_changes()
