from typing import Dict, List, Callable, Any
from fastapi import WebSocket, WebSocketDisconnect
import asyncio
import logging

from dependencies import EWM_Context

context = EWM_Context()

logging.getLogger("websockets").setLevel(logging.WARNING)


class WebSocketChannel:
    def __init__(self, get_current_data: Callable[[], Any], sleep_interval: float):
        self.clients: List[WebSocket] = []
        self.get_current_data = get_current_data
        self.sleep_interval = sleep_interval
        self.previous_data = None

    async def broadcast_data(self):
        while True:
            current_data = await self.get_current_data()
            # if current_data != self.previous_data:
            # state.log.debug("Sending update")
            for client in self.clients:
                await client.send_json(current_data)
                # self.previous_data = current_data
            # else:
            #     state.log.debug("Data is not changed")
            await asyncio.sleep(self.sleep_interval)

    async def connect(self, websocket: WebSocket):
        await websocket.accept()
        self.clients.append(websocket)
        if len(self.clients) == 1:
            context.log.debug("Starting broadcast task")
            self.task = asyncio.create_task(self.broadcast_data())

    async def disconnect(self, websocket: WebSocket):
        self.clients.remove(websocket)
        if not self.clients:
            context.log.debug("Stopping broadcast task")
            self.task.cancel()

    async def handle_connection(self, websocket: WebSocket):
        await self.connect(websocket)
        try:
            while True:
                await websocket.receive_text()
        except WebSocketDisconnect:
            await self.disconnect(websocket)
        except Exception as e:
            context.log.debug((f"{user.username} > Connection closed: {e}"))
