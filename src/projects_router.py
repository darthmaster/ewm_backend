from copy import deepcopy
from pathlib import Path
from shutil import rmtree
from tempfile import mkdtemp
from typing import Any, Dict
from zipfile import ZipFile

from classes import Project
from dependencies import EWM_Context, get_current_user
from fastapi import APIRouter, Depends, UploadFile
from fastapi.responses import FileResponse, JSONResponse
from fileproc import load_project, save_project

projects_router = APIRouter(dependencies=[Depends(get_current_user)])
context = EWM_Context()


@projects_router.get("/projects")
def get_projects_list():
    context.projects = []
    proj_dir = Path(context.path_projects)
    if proj_dir.exists():
        for path in proj_dir.iterdir():
            if path.is_dir():
                context.projects.append(load_project(path))
    return JSONResponse(content=[proj.name for proj in context.projects])


@projects_router.put("/projects")
def set_active_project(data: Dict[Any, Any]):
    context.active_project_name = data["project_name"]
    context.config.set("app", "ACTIVE_PROJECT", data["project_name"])
    context.log.info(f'Активный проект изменён на "{context.active_project_name}"')
    return load_project_from_dir(context.active_project_name)


@projects_router.delete("/projects")
def delete_project(data: Dict[Any, Any]):
    rmtree(f"{context.path_projects}/{data['project_name']}")
    context.log.info(f"Удалён проект \"{data['project_name']}\"")
    return get_projects_list()


@projects_router.post("/projects")
def create_project(data: Dict[Any, Any]):
    save_project(Project(name=data["project_name"]), context.path_projects)
    context.log.info(f"Создан новый проект \"{data['project_name']}\"")
    return get_projects_list()


@projects_router.get("/project")
def get_editable_project():
    return vars(context.editor_proj)


@projects_router.post("/project/modify")
def edit_project(data: Dict[Any, Any]):
    action = data["action"]

    if action == "add_bs":
        if context.editor_proj.stations:
            num = context.editor_proj.stations[-1].num + 1
        else:
            num = 1
        context.editor_proj.append_bs(num)

    elif action == "delete_bs":
        bs_id = data["bs_id"]
        del context.editor_proj.stations[bs_id]

    elif action == "set_bs_ip1":
        bs_id = data["bs_id"]
        ip = data["ip1"]
        context.editor_proj.stations[bs_id].ip1 = ip

    elif action == "set_bs_ip2":
        bs_id = data["bs_id"]
        ip = data["ip2"]
        context.editor_proj.stations[bs_id].ip2 = ip

    elif action == "set_bs_power":
        bs_id = data["bs_id"]
        power = data["power"]
        context.editor_proj.stations[bs_id].power = power

    elif action == "set_bsparam_state":
        bs_id = data["bs_id"]
        p_name = data["p_name"]
        p_state = data["state"]
        context.editor_proj.set_service_state(bs_id, p_name, p_state)

    elif action == "set_module":
        bs_id = data["bs_id"]
        mod_id = data["mod_id"]
        mod_type = data["mod_type"]
        context.editor_proj.set_module(bs_id, mod_id, mod_type)

    elif action == "set_module_params":
        bs_id = data["bs_id"]
        mod_id = data["mod_id"]
        params = data["params"]
        context.editor_proj.set_module_params(bs_id, mod_id, params)

    elif action == "set_proj_name":
        proj_name = data["name"]
        context.editor_proj.name = proj_name

    return vars(context.editor_proj)


@projects_router.get("/project/reload")
def load_project_from_dir(proj_name: str = context.ref_proj.name):
    context.active_project_name = proj_name
    context.config.set("app", "ACTIVE_PROJECT", proj_name)
    context.editor_proj = load_project(f"{context.path_projects}/{proj_name}")
    context.ref_proj = deepcopy(context.editor_proj)
    context.ref_proj.recalc_indices(summary=True)
    context.log.info(f"Активный проект перезагружен ({proj_name})")
    return vars(context.editor_proj)


@projects_router.get("/project/save")
def save_project_changes():
    save_project(context.editor_proj, context.path_projects)
    context.log.info(f"Проект сохранён ({context.editor_proj.name})")
    load_project_from_dir(context.editor_proj.name)
    return vars(context.editor_proj)


@projects_router.post("/project/import")
def import_project(project: UploadFile):
    TMP_DIR = mkdtemp()
    with ZipFile(project.file, "r") as zip_ref:
        zip_ref.extractall(TMP_DIR)
        context.editor_proj = load_project(TMP_DIR)
    context.log.info(f'Импортирован проект "{context.editor_proj.name}"')
    return vars(context.editor_proj)


@projects_router.get("/project/export")
def export_project_from_editor():
    TMP_DIR = mkdtemp()
    save_project(context.editor_proj, TMP_DIR)
    ZIP = f"{context.editor_proj.name}.zip"
    with ZipFile(ZIP, "w") as zip:
        for entity in Path(f"{TMP_DIR}/{context.editor_proj.name}").iterdir():
            if entity.is_dir() and entity.name.isdecimal():
                TMP_XML = f"{entity.as_posix()}/{context.xml_name}"
                TMP_DAT = f"{entity.as_posix()}/{context.dat_name}"
                ZIP_XML = f"{entity.name}/{context.xml_name}"
                ZIP_DAT = f"{entity.name}/{context.dat_name}"
                try:
                    zip.write(TMP_XML, ZIP_XML)
                    zip.write(TMP_DAT, ZIP_DAT)
                except Exception as e:
                    print(e)
            elif entity.name == "summary.xml":
                zip.write(entity.as_posix(), "summary.xml")
    context.log.info(f'Экспортирован проект "{context.editor_proj.name}"')
    return FileResponse(ZIP)
