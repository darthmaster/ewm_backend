import os
import sys
import subprocess

import uvicorn
from fastapi import FastAPI, APIRouter
from fastapi.middleware.cors import CORSMiddleware

from fastapi.openapi.docs import get_swagger_ui_html
from fastapi.staticfiles import StaticFiles
from dependencies import EWM_Context
from auth_router import auth_router
from device_router import device_router
from section_router import section_router
from arrays_router import arrays_router
from controller_router import controller_router
from utils_router import utils_router
from config_router import config_router
from consts_router import consts_router
from database import init_db


context = EWM_Context()

internal_router = APIRouter(prefix=context.api_prefix)
internal_router.include_router(auth_router, tags=["auth"])
internal_router.include_router(device_router, tags=["system"])
internal_router.include_router(section_router, tags=["section"])
internal_router.include_router(arrays_router, tags=["arrays"])
internal_router.include_router(controller_router, tags=["controller"])
internal_router.include_router(utils_router, tags=["system"])
internal_router.include_router(config_router, tags=["config"])
internal_router.include_router(consts_router, tags=["consts"])

app = FastAPI(
    title="EWM Backend",
    description="""
    ElnaWebManager backend API server
    
    WebSocket Endpoints
    > ws://localhost:8000/api/device?token=...
    > ws://localhost:8000/api/device/date?token=...
    > ws://localhost:8000/api/device/hwstat?token=...
    > ws://localhost:8000/api/section?token=...
    > ws://localhost:8000/api/section/module{pos}?token=...
    > ws://localhost:8000/api/controller?token=...
    > ws://localhost:8000/api/arrays-operations/read?token=...
    """,
    version="1.0.0rc17",
    docs_url=None,
)
app.mount("/static", StaticFiles(directory="static"))
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=False,
    allow_methods=["*"],
    allow_headers=["*"],
)
app.include_router(internal_router)


@app.get("/docs", include_in_schema=False)
async def custom_swagger_ui_html():
    return get_swagger_ui_html(
        openapi_url=app.openapi_url,  # type: ignore
        title=f"{app.title} - Swagger UI",
        swagger_css_url="/static/swagger-ui-modern.css",
        swagger_js_url="/static/swagger-ui-bundle.js",
    )


init_db()


def run():
    if not os.path.exists("/opt/Elna/ewm.crt") or not os.path.exists(
        "/opt/Elna/ewm.key"
    ):
        context.log.warning(
            "Не найдены файлы сертификата безопасности или ключа шифрования. Производится попытка генерации..."
        )
        cmd = [
            "openssl",
            "req",
            "-x509",
            "-nodes",
            "-days",
            "3650",
            "-newkey",
            "rsa:2048",
            "-keyout",
            "/opt/Elna/ewm.key",
            "-out",
            "/opt/Elna/ewm.crt",
            "-subj",
            '/C=RU/ST=Москва/L=Москва/O=ООО ВФ "ЭЛНА"/OU=СПО/CN=0.0.0.0',
            "-addext",
            "subjectAltName=IP:0.0.0.0",
            "-batch",
        ]
        try:
            result = subprocess.run(cmd, capture_output=True, text=True)
        except FileNotFoundError:
            context.log.error("Не удалось найти openssl в системе")
            sys.exit(1)
        else:
            if result.returncode == 0:
                context.log.warning(
                    "Сгенерирована новая пара сертификата безопасности или ключа шифрования"
                )
            else:
                context.log.error(
                    f"Возникла ошибка во время генерации: \n{result.stdout}\n{result.stderr}"
                )

    uvicorn.run(
        app,
        host="0.0.0.0",
        log_level="debug",
        workers=1,
        ssl_certfile="/opt/Elna/ewm.crt",
        ssl_keyfile="/opt/Elna/ewm.key",
        forwarded_allow_ips="*",
    )


if __name__ == "__main__":
    run()
