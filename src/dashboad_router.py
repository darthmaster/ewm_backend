from filecmp import cmp
from re import sub
from shutil import copyfile
from typing import Any, Dict, List, Tuple, Union
from zipfile import ZipFile

from classes import BS
from nettalk_client import download_file, remote_cmd, upload_file
from dependencies import EWM_Context, get_current_user
from fastapi import APIRouter, Depends, Response
from fastapi.responses import FileResponse
from fileproc import load_project
from ping3 import ping

dashboard_router = APIRouter(dependencies=[Depends(get_current_user)])
context = EWM_Context()

ModuleState = {
    0: "ERRM_ZERO",  # Ноль, не должно быть
    1: "NO_LOCAL_MODULE",  # Нет модуля в локальном проекте
    2: "NO_REMOTE_MODULE",  # Нет модуля в удаленном проекте
    3: "BAN_EXCHANGE_LOCAL",  # Запрет обмена в локальном проекте
    4: "BAN_EXCHANGE_REMOTE",  # Запрет обмена в удаленном проекте
    5: "STOPED_LOCAL_THREAD",  # Остановлен локальный поток
    6: "STOPED_REMOTE_THREAD",  # Остановлен удаленный поток
    7: "LOCAL_THREAD_HUNG",  # Завис локальный поток
    8: "REMOTE_THREAD_HUNG",  # Завис удаленный поток
    9: "NOT_CONNECTED",  # Не подключен
    10: "LOST",  # Нет связи
    11: "WRONG_MODULE_TYPE",  # Другой тип модуля в проекте
    12: "ZERO_DATAIN",  # Нулевые входные данные
    13: "INIT_RUN",  # Инициализация
    14: "PORT_NOT_OPENED",  # Порт не открыт
    15: "NETWORK_PROG_ERROR",  # Ошибка сетевой программы
    16: "INVALID_PROG_VERSION",  # Странная версия прошивки модуля
    20: "SUCCESS",  # Все хорошо, OK
}


def download_configs_in_use(ip, bs_num, is_main=False):
    host = (ip, context.nettalk_port)
    files = [
        (f".{bs_num}{context.xml_name}", f"{context.path_iousb}/{context.xml_name}"),
    ]
    if is_main:
        for i in range(len(context.ref_proj.stations) - 1):
            files.append(
                (
                    f".{i+1}{context.dat_name}",
                    f"{context.path_elpknet}/Server{i+1}/{context.dat_name}",
                )
            )
    else:
        files.append(
            (
                f".{bs_num}{context.dat_name}_client",
                f"{context.path_elpknet}/Client1/{context.dat_name}",
            )
        )
    return all(download_file(host, src, dest) for src, dest in files)


def try_ping(ip, bs_num):
    online = None
    if ip:
        try:
            online = ping(ip, timeout=1, unit="ms")
            if online:
                configs_downloaded = download_configs_in_use(
                    ip, bs_num, context.ref_proj.stations[0].num == bs_num
                )
                return online, not configs_downloaded
        except ConnectionRefusedError as e:
            context.log.error(e)
            return (
                online,
                "в подключении отказано (не запущен nettalk или неверный крипто-ключ)",
            )
        except OSError as e:
            context.log.error(e)
            return online, "нет маршрута до адреса, указанного в проекте"
    return None, None


# УСТАРЕЛО
def check_configuration(bs: BS):
    return cmp(
        f".{bs.num}{context.xml_name}",
        f"{context.path_projects}/{context.active_project_name}/{bs.num}/{context.xml_name}",
    )


def get_config_status(bsnum: int, isMain: bool):
    # Список конфигурационных файлов
    local_files = [f".{bsnum}{context.xml_name}"]
    if isMain:
        servers_count = 0
        for bs in context.ref_proj.stations:
            if bs.num != bsnum:
                servers_count += 1
                local_files.append(f".{servers_count}{context.dat_name}")
    else:
        local_files.append(f".{bsnum}{context.dat_name}_client")
    # Список для хранения результатов
    results = []
    # Цикл по файлам
    for file in local_files:
        # Словарь для хранения информации о файле
        file_info = {}
        # Устанавливаем имя файла
        file_info["name"] = file
        # Пытаемся открыть файл из проекта
        project_file_name = sub(r"\d+|_client", "", file[1:])
        try:
            with open(
                f"{context.path_projects}/{context.ref_proj.name}/{bsnum}/{project_file_name}"
            ) as project_file:
                # Читаем содержимое файла из проекта
                project_content = project_file.read()
        except FileNotFoundError:
            # Если файл из проекта не найден, то устанавливаем статус в "нет данных" и продолжаем цикл
            file_info["status"] = "нет данных"
            results.append(file_info)
            continue
        # Пытаемся открыть файл из локальной папки
        try:
            with open(file) as local_file:
                # Читаем содержимое файла из локальной папки
                local_content = local_file.read()
        except FileNotFoundError:
            # Если файл из локальной папки не найден, то устанавливаем статус в "нет данных" и продолжаем цикл
            file_info["status"] = "нет данных"
            results.append(file_info)
            continue
        # Сравниваем содержимое файлов
        if project_content == local_content:
            # Если содержимое файлов совпадает, то устанавливаем статус в "совпадает с проектом"
            file_info["status"] = "совпадает с проектом"
        else:
            # Если содержимое файлов отличается, то устанавливаем статус в "отличается от проекта"
            file_info["status"] = "отличается от проекта"
        # Устанавливаем ссылку на скачивание файла
        file_info["downloadLink"] = (
            f"/api/config/download?bsnum={bsnum}&file_name={file}"
        )
        # Устанавливаем ссылку на сравнение файла, если статус неактуальный
        if file_info["status"] == "отличается от проекта":
            file_info["compareLink"] = (
                f"/api/config/compare?bsnum={bsnum}&file_name={file}"
            )
        # Добавляем информацию о файле в список результатов
        results.append(file_info)
    return results


def get_modules_state(bs: BS) -> List[tuple]:
    result = []
    for module in bs.modules:
        state_code = module.read(context.elpk, "ERR", 0)["ERR"][0]["d_value"]
        result.append(
            (
                module.str_type,
                ModuleState.get(state_code, f"Неизвестный код статуса: {state_code}"),
            )
        )
    return result


@dashboard_router.get("/get_stats")
def get_stats():
    result = {}
    for bs in context.ref_proj.stations:
        ip1_online, err_msg1 = try_ping(bs.ip1, bs.num)
        ip2_online, err_msg2 = try_ping(bs.ip2, bs.num)
        err_msg = err_msg1 or err_msg2

        config_status = None
        try:
            if ip1_online or ip2_online:  # and not err_msg:
                err_msg = ""
                # conf_eq_proj = check_configuration(bs)
                config_status = get_config_status(
                    bs.num, bs.num == context.ref_proj.stations[0].num
                )
        except Exception as e:
            context.log.error(e)

        result.update(
            {
                f"БС-{bs.num}": {
                    "num": bs.num,
                    # "is_main": is_main,
                    "ip1": bs.ip1,
                    "ip2": bs.ip2,
                    "ip1_online": ip1_online,
                    "ip2_online": ip2_online,
                    # "configured": conf_eq_proj,
                    "configs_stat": config_status,
                    "err": err_msg,
                    "modules": get_modules_state(bs),
                }
            }
        )
    return result


@dashboard_router.post("/get_cmd_out")
def exec_bs_cmd(data: Dict[Any, Any]):
    result = "нет данных"
    try:
        HOST = (data["ip1"], context.nettalk_port)
        CMD = tuple(context.spellbook.values())[int(data["cmd"])]
        result = remote_cmd(HOST, CMD)
    except ConnectionRefusedError:
        result = "в подключении отказано (не запущен nettalk или неверный крипто-ключ)"
    except OSError:
        result = "нет маршрута до адреса, указанного в проекте"
    return result


# УСТАРЕЛО
@dashboard_router.get("/download")
def download_confs(bsnum):
    with ZipFile(f"BS{bsnum}.zip", "w") as zip:
        zip.write(f".{bsnum}{context.xml_name}", context.xml_name)
        zip.write(f".{bsnum}{context.dat_name}", context.dat_name)
    context.log.info(
        f"Экспортированы используемые конфигурацонные файлы БС-{bsnum} активного проекта ({context.active_project_name})"
    )
    return FileResponse(f"BS{bsnum}.zip")


# Метод для скачивания конфигурационных файлов
@dashboard_router.get("/config/download")
def download_config_file(bsnum: str, file_name: str):
    # # Пытаемся открыть файл из проекта
    # try:
    #     project_file_name = sub('\d+|_client', '', file_name[1:])
    #     with open(f"{state.path_projects}/{state.ref_proj.name}/{bsnum}/{project_file_name}") as project_file:
    #         # Читаем содержимое файла из проекта
    #         project_content = project_file.read()
    # except FileNotFoundError:
    #     # Если файл из проекта не найден, то возвращаем ошибку 404
    #     return Response(status_code=404)
    # # Возвращаем содержимое файла в формате plain text
    parsed_name = f"(БС-{bsnum})" + sub(r"\d+|_client", "", file_name[1:])
    return FileResponse(path=file_name, filename=parsed_name)


# УСТАРЕЛО
@dashboard_router.get("/compare")
def get_bs_configs(bsnum):
    from_project = open(
        f"{context.path_projects}/{context.ref_proj.name}/{bsnum}/{context.xml_name}"
    ).read()
    from_device = open(f".{bsnum}{context.xml_name}").read()
    return {"device": from_device, "project": from_project}


# Метод для сравнения содержимого конфигурационных файлов
@dashboard_router.get("/config/compare")
def compare_config_file(bsnum: str, file_name: str):
    # Пытаемся открыть файл из проекта
    try:
        project_file_name = sub(r"\d+|_client", "", file_name[1:])
        with open(
            f"{context.path_projects}/{context.ref_proj.name}/{bsnum}/{project_file_name}"
        ) as project_file:
            # Читаем содержимое файла из проекта
            project_content = project_file.read()
    except FileNotFoundError:
        # Если файл из проекта не найден, то возвращаем ошибку 404
        return Response(status_code=404)
    # Пытаемся открыть файл из локальной папки
    try:
        with open(file_name) as local_file:
            # Читаем содержимое файла из локальной папки
            local_content = local_file.read()
    except FileNotFoundError:
        # Если файл из локальной папки не найден, то возвращаем ошибку 404
        return Response(status_code=404)
    # Словарь для хранения результатов
    results = {}
    # Устанавливаем имя файла
    results["name"] = file_name
    # Устанавливаем старое и новое содержимое файла
    results["oldContent"] = local_content
    results["newContent"] = project_content
    # Возвращаем результаты в формате JSON
    return results


# УСТАРЕЛО
@dashboard_router.get("/load_from_project")
def load_from_project(bsnum):
    bs = context.ref_proj.find_by_num(int(bsnum))
    if bs:
        HOST = (bs.ip1, 7219)
        err_msg = []
        result = []
        try:
            XML_PROJ = (
                f"{context.path_projects}/{context.ref_proj.name}/{bs.num}/{context.xml_name}"
            )
            XML_WORK = f"{context.path_iousb}/{context.xml_name}"
            result.append(upload_file(HOST, XML_PROJ, XML_WORK))
            context.log.info(
                f"Произведена замена используемых конфигурационных файлов БС-{bsnum} на файлы активного проекта ({context.active_project_name})"
            )
        except Exception as e:
            err_msg.append(str(e))
            context.log.error(e)
        try:
            DAT_PROJ = (
                f"{context.path_projects}/{context.ref_proj.name}/{bs.num}/{context.dat_name}"
            )
            DAT_WORK = f"{context.path_iousb}/{context.dat_name}"
            result.append(upload_file(HOST, DAT_PROJ, DAT_WORK))
            context.log.info(
                f"Произведена замена используемых конфигурационных файлов БС-{bsnum} на файлы активного проекта ({context.active_project_name})"
            )
        except Exception as e:
            if err_msg[-1] != str(e):
                err_msg.append(str(e))
            context.log.error(e)
        return {"result": result, "err": err_msg}
    else:
        return None


@dashboard_router.get("/config/load_from_project")
def replace_in_workdir(bsnum: str, filename: str):
    project_file_name = sub(r"\d+|_client", "", filename[1:])
    bs = context.ref_proj.find_by_num(int(bsnum))
    if bs:
        HOST = (bs.ip1, 7219)
        err_msg = []
        result = []
        try:
            PROJECT_PATH = f"{context.path_projects}/{context.ref_proj.name}/{bsnum}/{project_file_name}"
            if filename.find(context.xml_name) > 0:
                WORKING_PATH = f"{context.path_iousb}/{context.xml_name}"
            elif filename.find("_client") > 0:
                WORKING_PATH = f"{context.path_elpknet}/Client1/{context.dat_name}"
            else:
                for index, bs in enumerate(context.ref_proj.stations):
                    if str(index + 1) == filename[1 : filename.find(context.dat_name)]:
                        PROJECT_PATH = f"{context.path_projects}/{context.ref_proj.name}/{bs.num}/{project_file_name}"
                        break
                WORKING_PATH = f"{context.path_elpknet}/Server{filename[1:filename.find(context.dat_name)]}/{context.dat_name}"

            result.append(upload_file(HOST, PROJECT_PATH, WORKING_PATH))
            context.log.info(
                f"Произведена замена {WORKING_PATH} БС-{bsnum} на экземпляр из активного проекта ({context.active_project_name})"
            )
        except Exception as e:
            err_msg.append(str(e))
            context.log.error(e)
        return {"result": result, "err": err_msg}
    else:
        return None


# УСТАРЕЛО
@dashboard_router.get("/save_to_project")
def save_to_project(bsnum):
    err_msg = []
    result = []
    try:
        XML_WORK = f".{bsnum}{context.xml_name}"
        XML_PROJ = (
            f"{context.path_projects}/{context.ref_proj.name}/{bsnum}/{context.xml_name}"
        )
        result.append(copyfile(XML_WORK, XML_PROJ))
        context.log.info(
            f"Произведена замена файлов активного проекта ({context.active_project_name}) на используемые конфигурационные файлы БС-{bsnum}"
        )
    except Exception as e:
        err_msg.append(str(e))
        context.log.error(e)
    try:
        DAT_WORK = f".{bsnum}{context.dat_name}"
        DAT_PROJ = (
            f"{context.path_projects}/{context.ref_proj.name}/{bsnum}/{context.dat_name}"
        )
        result.append(copyfile(DAT_WORK, DAT_PROJ))
        context.log.info(
            f"Произведена замена файлов активного проекта ({context.active_project_name}) на используемые конфигурационные файлы БС-{bsnum}"
        )
    except Exception as e:
        if err_msg[-1] != str(e):
            err_msg.append(str(e))
        context.log.error(e)
    context.ref_proj = context.editor_proj = load_project(
        f"{context.path_projects}/{context.active_project_name}"
    )
    return {"result": result, "err": err_msg}


@dashboard_router.get("/config/save_to_project")
def replace_in_project(bsnum: str, filename: str):
    project_file_name = sub(r"\d+|_client", "", filename[1:])
    err_msg = []
    result = []
    try:
        target = (
            f"{context.path_projects}/{context.ref_proj.name}/{bsnum}/{project_file_name}"
        )
        result.append(copyfile(filename, target))
        context.log.info(
            f"Произведена замена {project_file_name} активного проекта ({context.active_project_name}) на используемый в работе БС-{bsnum}"
        )
    except Exception as e:
        err_msg.append(str(e))
        context.log.error(e)
    context.ref_proj = context.editor_proj = load_project(
        f"{context.path_projects}/{context.active_project_name}"
    )
    return {"result": result, "err": err_msg}


@dashboard_router.get("/get_module")
def get_module(bsnum: int, modpos: int):
    bs = context.ref_proj.find_by_num(bsnum)
    if bs:
        module = bs.modules[modpos]
        module.read(elpk=context.elpk)
        return {
            "name": module.str_type,
            "type": module.int_type,
            "params": module.params,
            "arrays_info": module.arrays_info,
            "arrays_data": module.data,
        }


@dashboard_router.get("/get_module_data")
def get_module_data(bsnum: int, modpos: int):
    bs = context.ref_proj.find_by_num(bsnum)
    if bs:
        module = bs.modules[modpos]
        module.read(elpk=context.elpk)
        return module.data
    return {"details": "БС с таким номером нет в проекте"}


@dashboard_router.post("/set_module_data")
def set_module_data(data: Dict[Any, Any]):
    bs = context.ref_proj.find_by_num(data.pop("bsnum"))
    if bs:
        module = bs.modules[data.pop("modpos")]
        module.write(elpk=context.elpk, **data)
        return module.data
    return {"details": "БС с таким номером нет в проекте"}


def try_remote_cmd(ip, cmd) -> Tuple[bool, str]:
    try:
        return True, remote_cmd((ip, context.nettalk_port), cmd)
    except ConnectionRefusedError:
        return (
            False,
            "в подключении отказано (не запущен nettalk или неверный крипто-ключ)",
        )
    except OSError:
        return False, "нет маршрута до адреса, указанного в проекте"


def execute_command_on_bs(
    ip_list: List[str], cmd: str, log_msg: str
) -> List[Union[str, None]]:
    _first_try, _second_try = [None, None], [None, None]

    _first_try = try_remote_cmd(ip_list[0], cmd)
    if _first_try[0]:
        context.log.warning(f"{log_msg} на {ip_list[0]}")
    else:
        context.log.error(_first_try[1])
        _second_try = try_remote_cmd(ip_list[1], cmd)
        if _second_try[0]:
            context.log.warning(f"{log_msg} на {ip_list[1]}")
        else:
            context.log.error(_second_try[1])

    return [_first_try[1], _second_try[1]]


@dashboard_router.post("/restart_services")
def restart_services(bs_ip: List[Any]):
    # summary = []
    # summary.append(
    #     execute_command_on_bs(
    #         bs_ip,
    #         "systemctl restart ewm_frontend.service",
    #         "Перезапускается ewm_frontend",
    #     )
    # )
    # summary.append(
    #     execute_command_on_bs(
    #         bs_ip,
    #         "systemctl restart ewm_backend.service ",
    #         "Перезапускается ewn_backend",
    #     )
    # )
    # summary.append(
    #     execute_command_on_bs(
    #         bs_ip, "systemctl restart nettalk.service ", "Перезапускается nettalk"
    #     )
    # )
    return execute_command_on_bs(
        bs_ip,
        "systemctl restart ewm_frontend ewm_backend nettalk",
        "Отправлена команда перезапуска сервисов",
    )


@dashboard_router.post("/reboot_os")
def reload_os(bs_ip: List[Any]):
    return execute_command_on_bs(bs_ip, "reboot", "Команда перезагрузки отправлена")


@dashboard_router.post("/shutdown")
def shutdown_bs(bs_ip: List[Any]):
    return execute_command_on_bs(bs_ip, "poweroff", "Команда выключения отправлена")
