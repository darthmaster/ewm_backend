import ctypes as ct

TypeIO = {
    "DIN": 1,
    "DOUT": 2,
    "AIN": 3,
    "AOUT": 4,
    "CIRC": 5,
    "RelSt": 6,
    "AOIN": 7,
    "ERR": 8,
    "SD": 9,
    "AIW": 10,
    "SCSC": 11,
}
TypeIO_inv = {
    1: "DIN",
    2: "DOUT",
    3: "AIN",
    4: "AOUT",
    5: "CIRC",
    6: "RelSt",
    7: "AOIN",
    8: "ERR",
    9: "SD",
    10: "AIW",
    11: "SCSC",
}


class ElpkWrapper:
    def __init__(self, legacy: bool = False, location="./"):
        if legacy:
            self.analog_type = ct.c_double
            self.library_name = "libelpk4.64_lw.so"
        else:
            self.analog_type = ct.c_float
            self.library_name = "libelpk4.64_w.so"

        ServiceData_attrs = {
            "_fields_": [
                ("gpio", ct.c_ushort * 2),
                ("life", ct.c_ushort * 2),
                ("ain", self.analog_type * 8),
                ("f_ain", ct.c_ushort * 8),
                ("f_life", ct.c_ushort * 2),
                ("f_gpio", ct.c_ushort * 2),
            ]
        }
        self.ServiceData = type("ServiceData", (ct.Structure,), ServiceData_attrs)

        ChannelData_attrs = {
            "_fields_": [
                ("flag", ct.c_ushort),
                ("d_value", ct.c_ushort),
                ("a_value", self.analog_type),
            ],
            "__repr__": lambda self: f"ChD[ f={self.flag} dv={self.d_value} av={self.a_value} ]",
        }
        self.ChannelData = type("ChannelData", (ct.Structure,), ChannelData_attrs)

        wrapper = ct.cdll.LoadLibrary(name=location + self.library_name)

        self.lib_read_channel = wrapper.ReadChannel
        self.lib_read_channel.argtypes = [ct.c_int, ct.c_int]
        self.lib_read_channel.restype = self.ChannelData

        self.lib_write_channel = wrapper.WriteChannel
        self.lib_write_channel.argtypes = [ct.c_int, ct.c_int, self.ChannelData]

        self.lib_read_service = wrapper.ReadService
        self.lib_read_service.argtypes = [ct.c_int]
        self.lib_read_service.restype = self.ServiceData

        self.lib_write_service = wrapper.WriteService
        self.lib_write_service.argtypes = [ct.c_int, self.ServiceData]

    def read_service(self, index):
        return self.lib_read_service(index)

    def write_service(self, index, data):
        return self.lib_write_service(index, data)

    def read_channel(self, array_name, index):
        return self.lib_read_channel(TypeIO.get(array_name), index)

    def write_channel_by_name(self, array_name, index, value, flag):
        data = self.ChannelData()
        data.flag = int(flag)
        if array_name in ("AIN", "AOUT", "AOIN"):
            data.a_value = float(value)
        else:
            data.d_value = int(value)
        return self.lib_write_channel(TypeIO.get(array_name), index, data)

    def write_channel_by_id(self, arr_id, index, data):
        return self.lib_write_channel(arr_id, index, data)


class Colors:
    """ANSI color codes"""

    BLACK = "\033[0;30m"
    RED = "\033[0;31m"
    GREEN = "\033[0;32m"
    BROWN = "\033[0;33m"
    BLUE = "\033[0;34m"
    PURPLE = "\033[0;35m"
    CYAN = "\033[0;36m"
    LIGHT_GRAY = "\033[0;37m"
    DARK_GRAY = "\033[1;30m"
    LIGHT_RED = "\033[1;31m"
    LIGHT_GREEN = "\033[1;32m"
    YELLOW = "\033[1;33m"
    LIGHT_BLUE = "\033[1;34m"
    LIGHT_PURPLE = "\033[1;35m"
    LIGHT_CYAN = "\033[1;36m"
    LIGHT_WHITE = "\033[1;37m"
    BOLD = "\033[1m"
    FAINT = "\033[2m"
    ITALIC = "\033[3m"
    UNDERLINE = "\033[4m"
    BLINK = "\033[5m"
    NEGATIVE = "\033[7m"
    CROSSED = "\033[9m"
    END = "\033[0m"
