from typing import Union, Optional, List
from pydantic import BaseModel


class SoftwareUpgradeRequest(BaseModel):
    full: bool = False
    backend: bool = False
    frontend: bool = False
    system: bool = False


class SectionPowerChannelSetup(BaseModel):
    channel: int
    enabled: bool
    inverted: bool


class FilePath(BaseModel):
    path: str


class SectionRemoveRequest(BaseModel):
    section_id: int


class SectionConfigStatus(BaseModel):
    serial_number: bool = False
    bsconfig: bool = False
    servertcp: bool = False


class SectionStatus(BaseModel):
    bs_number: Union[str, None] = None
    lan1: Union[str, None] = None
    lan2: Union[str, None] = None
    lan1_online: Union[bool, None] = None
    lan2_online: Union[bool, None] = None
    serial_number: Optional[str] = None
    config_status: Optional[SectionConfigStatus] = None


class ControllerStatus(BaseModel):
    bs_number: Union[str, None] = None
    role: str = ""
    lan1: str = ""
    lan2: str = ""
    serial_number: str = ""
    master: Optional[SectionStatus] = None
    slaves: Optional[List[SectionStatus]] = None


class ServiceRequest(BaseModel):
    service_name: str
    action: str  # например: start, stop, restart, status


class Section(BaseModel):
    lan1: str = ""
    lan2: str = ""
    serial_number: str = ""


class SectionConnectionRequest(Section):
    section_id: Optional[Union[int, None]] = None


class SectionConnectionResult(BaseModel):
    status: Optional[bool] = None
    serial_match: Optional[bool] = None
    online: Optional[List[bool]] = None
    details: str = ""


class ModuleWriteData(BaseModel):
    array: str
    index: int
    value: Union[int, float]
    flag: int = 0xC0


class TimesyncParams(BaseModel):
    service_enabled: bool
    server_address: str
    server_mode: bool
    subnet_address: str


class DeviceSetupParams(BaseModel):
    timesync: Union[TimesyncParams, None] = None
    date: Union[str, None] = None
    hostname: Union[str, None] = None
    lan1: Union[str, None] = None
    lan2: Union[str, None] = None

    model_config = {
        "json_schema_extra": {
            "examples": [
                {
                    "timesync": {
                        "service_enabled": True,
                        "server_address": "192.168.0.1",
                        "server_mode": True,
                        "subnet_address": "192.168.10.0/24",
                    },
                    "date": "2024-01-01 00:00:00",
                    "hostname": "bs1",
                    "lan1": "192.168.10.100/24",
                    "lan2": "192.168.11.100/24",
                },
            ]
        }
    }


class DeviceSetupResult(BaseModel):
    set_chronyd_service_status: Union[tuple[bool, str], None] = None
    edit_chrony_conf: Union[tuple[bool, str], None] = None
    set_date: Union[tuple[bool, str], None] = None
    set_hostname: Union[tuple[bool, str], None] = None
    set_lan1_ip: Union[tuple[bool, str], None] = None
    set_lan2_ip: Union[tuple[bool, str], None] = None
    set_device_params: Union[tuple[bool, str], None] = None

    model_config = {
        "json_schema_extra": {
            "examples": [
                {
                    "set_chronyd_service_status": [
                        True,
                        'Created symlink / Removed "путь_к_сервису"',
                    ],
                    "edit_chrony_conf": [True, "chrony.conf изменён"],
                    "set_date": [True, "Установлена новая дата"],
                    "set_hostname": [True, "Имя машины изменено"],
                    "set_lan1_ip": [True, "Установлен новый адрес"],
                    "set_lan2_ip": [True, "Установлен новый адрес"],
                },
                {"set_device_params": [False, "текст_ошибки"]},
            ]
        }
    }


class Token(BaseModel):
    access_token: str
    token_type: str


class TokenData(BaseModel):
    username: Optional[str] = None


class UserBase(BaseModel):
    username: str


class UserCreate(UserBase):
    password: str


class User(UserBase):
    id: int

    class Config:
        orm_mode = True


class UserPasswordUpdate(BaseModel):
    old_password: str
    new_password: str
