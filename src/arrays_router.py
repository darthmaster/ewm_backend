from typing import Any, Dict
import asyncio

from fastapi import APIRouter, Depends, WebSocket
from fastapi.responses import JSONResponse

from schemas import User
from classes import DataRW
from WebSocketChannel import WebSocketChannel
from dependencies import EWM_Context, get_current_user, get_current_user_ws

arrays_router = APIRouter(prefix="/arrays-operations")
context = EWM_Context()


def cards_data():
    cards_data = []
    for card in context.cards.values():
        cards_data.append(card.to_dict())
    return cards_data


@arrays_router.get("")
async def get_aos(user: User = Depends(get_current_user)):
    return cards_data()


@arrays_router.delete("")
async def delete_ao(data: Dict[Any, Any], user: User = Depends(get_current_user)):
    context.cards.pop(data["id"])
    return cards_data()


@arrays_router.post("")
async def add_ao(data: Dict[Any, Any], user: User = Depends(get_current_user)):
    card = DataRW(id=data["id"])
    context.cards.update({card.id: card})
    return cards_data()


@arrays_router.get("/read")
async def read_arrays():
    result = []
    for card in context.cards.values():
        read_result = context.elpk.read_channel(card.array, int(card.index))
        card.flag = read_result.flag
        if card.array in ("AIN", "AOUT", "AOIN"):
            card.value = round(read_result.a_value, 2)
        else:
            card.value = read_result.d_value
        result.append(card.to_dict())
    return result


arrays_channel = WebSocketChannel(read_arrays, 0.2)


@arrays_router.websocket("/read")
async def read_arrays_cycle(ws: WebSocket, user: User = Depends(get_current_user_ws)):
    await arrays_channel.handle_connection(ws)


@arrays_router.put("")
async def edit_aos(data: Dict[Any, Any], user: User = Depends(get_current_user)):
    card_id = data["id"]
    context.cards[card_id].id = card_id
    # context.cards[card_id].mode = data["mode"]
    context.cards[card_id].array = data["array"]
    context.cards[card_id].index = data["index"]
    # context.cards[card_id].value = data["value"]
    # context.cards[card_id].flag = data["flag"]
    return await read_arrays()  # JSONResponse(content=context.cards[card_id].to_dict())


@arrays_router.put("/write")
async def write_to_index(data: Dict[Any, Any], user: User = Depends(get_current_user)):
    card_id = data["id"]
    try:
        index = int(data["index"])
        flag = int(data["flag"])
        if data["array"] in ("AIN", "AOUT", "AOIN"):
            val = float(data["value"])
        else:
            val = int(data["value"])
    except (ValueError, TypeError):
        error_text = "Введено некорректное знчение для записи"
        return {"err": error_text, "card_id": card_id}
    context.elpk.write_channel_by_name(data["array"], index, val, flag)
    return await read_arrays()  # JSONResponse(content=context.cards[card_id].to_dict())
