#!/usr/bin/env python3
import os
import socket
import time
from typing import Union

key = None
connections = []


def load_key(filename: str) -> bool:
    global key
    if not os.path.exists(filename):
        return False
    f = open(filename, "rb")
    key = f.read()
    f.close()
    return True


def crypto(buf: bytes, key_pos: int) -> tuple:
    if not key:
        return (buf, 0)
    len_key = len(key)
    res = bytearray(len(buf))
    for i in range(len(buf)):
        res[i] = buf[i] ^ key[key_pos]
        key_pos += 1
        if key_pos == len_key:
            key_pos = 0

    res = bytes(res)
    return (res, key_pos)


def remote_cmd(host: tuple, cmd: str) -> str:
    """
    host кортеж из 2х элементов: (ip_addres: str, port: int)
    cmd команда. Не должно быть перенаправление, типа |
    - ps aux # правильно
    - ps aux | grep -v ps # вернет пустую строку
    return: str
    - возвращает stdout
    """
    buf = list()
    s = socket.socket()
    s.settimeout(0.5)
    s.connect(host)

    key_pos = 0
    load_key("crypto.key")
    r, key_pos = crypto(("cmd:" + cmd).encode(), 0)
    s.send(r)
    key_pos = 0
    while True:
        r = s.recv(1400)
        r, key_pos = crypto(r, key_pos)
        i = len(r)
        i = r[i - 5 :]
        if i == b"-EOF-" or len(r) == 0:
            r = r[:-5]
            buf.append(r)
            break
        buf.append(r)

    buf = b"".join(buf).decode()

    s.close()
    return buf


def upload_file(host: tuple, src_filename: str, dst_filename: str) -> bool:
    if not os.path.exists(src_filename):
        return False
    if not os.path.isfile(src_filename):
        return False
    key_pos = 0
    load_key("crypto.key")
    with open(src_filename, "rb") as f:
        s = socket.socket()
        s.settimeout(0.5)
        s.connect(host)
        dst_filename = "download:" + dst_filename
        buf, key_pos = crypto(dst_filename.encode(), key_pos)
        s.send(buf)
        time.sleep(0.5)
        key_pos = 0
        while True:
            buf = f.read(1400)
            buf, key_pos = crypto(buf, key_pos)
            if not buf:
                buf, key_pos = crypto(b"-EOF-", key_pos)
                s.send(buf)
                break
            s.send(buf)
    s.close()
    return True


def download_file(host: tuple, src_filename: str, dst_filename: str) -> bool:
    load_key("crypto.key")
    f = open(src_filename, "wb")
    s = socket.socket()
    s.settimeout(0.5)
    s.connect(host)
    dst_filename = "upload:" + dst_filename
    i = 0
    buf, key_pos = crypto(dst_filename.encode(), 0)
    s.send(buf)
    key_pos = 0
    while True:
        buf = s.recv(1400)
        buf, key_pos = crypto(buf, key_pos)
        if not buf or buf == b"-EOF-":
            if i == 0:
                f.close()
                os.remove(src_filename)
                return False
        i = len(buf) - 5
        if buf[i:] == b"-EOF-":
            buf = buf[:-5]
            f.write(buf)
            f.close()
            return True
        f.write(buf)


def remote_cmds(host: tuple, cmds: list) -> list:
    """
    host кортеж из 2х элементов: (ip_addres: str, port: int)
    cmd команда. Не должно быть перенаправление, типа |
    - ps aux # правильно
    - ps aux | grep -v ps # вернет пустую строку
    return: str
    - возвращает stdout
    """
    ret = list()

    s = socket.socket()
    # s.settimeout(0.5)
    s.connect(host)

    load_key("crypto.key")
    key_pos = 0
    for l in cmds:
        buf = list()
        r, key_pos = crypto(("cmd:" + l).encode(), key_pos)
        s.send(r)
        key_pos = 0

        while True:
            r = s.recv(1400)
            r, key_pos = crypto(r, key_pos)
            i = len(r)
            i = r[i - 5 :]
            if i == b"-EOF-" or len(r) == 0:
                r = r[:-5]
                buf.append(r)
                break
            buf.append(r)

        buf = b"".join(buf).decode()
        ret.append(buf)

    s.close()
    return ret


def rconnect(host: tuple) -> socket.socket:
    global _key_pos
    global connections
    s = socket.socket()
    # s.settimeout(0.5)
    s.connect(host)
    load_key("crypto.key")
    connections.append({s: 0})
    return s


def find_pos(s: socket.socket) -> int:
    for i in range(len(connections)):
        if connections[i].get(s) != None:
            return i
    return -1


def remote_command(s: socket.socket, cmd: str) -> Union[str, None]:
    global connections

    key_pos = find_pos(s)
    if key_pos < 0:
        s.close()
        return None
    key_pos = connections[key_pos][s]

    r, key_pos = crypto(("cmd:" + cmd).encode(), key_pos)
    buf = list()
    s.send(r)
    key_pos = 0
    while True:
        r = s.recv(1400)
        r, key_pos = crypto(r, key_pos)
        i = len(r)
        i = r[i - 5 :]
        if i == b"-EOF-" or len(r) == 0:
            r = r[:-5]
            buf.append(r)
            break
        buf.append(r)

    buf = b"".join(buf).decode()
    r = find_pos(s)
    connections[r][s] = key_pos
    return buf


def disconnect(s: socket.socket):
    global connections
    i = find_pos(s)
    del connections[i]
    s.close()


if __name__ == "__main__":
    exit(0)
