from dependencies import get_current_user
from fastapi import APIRouter, Depends

consts_router = APIRouter(prefix="/consts", dependencies=[Depends(get_current_user)])


@consts_router.get("/module-types")
async def get_module_types():
    return [
        {"name": "IA-4k42-M", "code": 2002},
        {"name": "IA-8k42", "code": 2800},
        {"name": "ID-16k24", "code": 6002},
        {"name": "ID-8k24-M", "code": 4002},
        {"name": "IF-3k", "code": 1903},
        {"name": "OA-4k42-M", "code": 3002},
        {"name": "OD-16k24", "code": 1600},
        {"name": "OD-5k-M", "code": 1502},
        {"name": "RS-485-4k", "code": 20},
        {"name": "Пусто", "code": 0},
    ]


@consts_router.get("/module-states")
async def get_module_states():
    return {
        0: "ERRM_ZERO",  # Ноль, не должно быть
        1: "NO_LOCAL_MODULE",  # Нет модуля в локальном проекте
        2: "NO_REMOTE_MODULE",  # Нет модуля в удаленном проекте
        3: "BAN_EXCHANGE_LOCAL",  # Запрет обмена в локальном проекте
        4: "BAN_EXCHANGE_REMOTE",  # Запрет обмена в удаленном проекте
        5: "STOPED_LOCAL_THREAD",  # Остановлен локальный поток
        6: "STOPED_REMOTE_THREAD",  # Остановлен удаленный поток
        7: "LOCAL_THREAD_HUNG",  # Завис локальный поток
        8: "REMOTE_THREAD_HUNG",  # Завис удаленный поток
        9: "NOT_CONNECTED",  # Не подключен
        10: "LOST",  # Нет связи
        11: "WRONG_MODULE_TYPE",  # Другой тип модуля в проекте
        12: "ZERO_DATAIN",  # Нулевые входные данные
        13: "INIT_RUN",  # Инициализация
        14: "PORT_NOT_OPENED",  # Порт не открыт
        15: "NETWORK_PROG_ERROR",  # Ошибка сетевой программы
        16: "INVALID_PROG_VERSION",  # Странная версия прошивки модуля
        20: "SUCCESS",  # Все хорошо, OK
    }


@consts_router.get("/array-types")
async def get_array_types():
    return {
        1: "DIN",
        2: "DOUT",
        3: "AIN",
        4: "AOUT",
        5: "CIRC",
        6: "RelSt",
        7: "AOIN",
        8: "ERR",
        9: "SD",
        10: "AIW",
        11: "SCSC",
    }


@consts_router.get("/opc-qualities")
async def get_opc_qualities():
    return {
        0x00: "NO_QUALITY",  # Ноль, нет качества, недостоверно
        0x04: "CONFIG_FAIL",  # Ошибка конфигурации
        0x08: "NOT_CONNECTED",  # Не подключен
        0x0C: "DEVICE_FAILURE",  # Ошибка устройства
        0x10: "SENSOR_FAILURE",  # Ошибка датчика
        0x14: "LAST_VALID",  # Используется последнее достоверное значение
        0x18: "COMM_FAILURE",  # Нет связи
        0x1C: "OUT_OF_SERVICE",  # Не обслуживается
        0x20: "INIT_RUN",  # Выполняется инициализация
        0xC0: "SUCCESS",  # Все хорошо, OK
    }
