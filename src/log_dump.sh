#!/bin/bash

TM_BASE="/tmp/elna-telemetry"
TM_DIR="${TM_BASE}/$(date '+%s')"
UARCH="tm-$(date '+%d-%m-%Y_%H-%M-%s').tar.gz"

log_dir=(
    "/var/log/elna"
    "/var/log/elpk4"
    "/opt/mplc4/log"
    "/opt/Elna/io.usb/log"
    "/home/user/Server1/log"
    "/home/user/Server2/log"
    "/home/user/Server3/log"
    "/home/user/Server4/log"
    "/home/user/Server5/log"
    "/home/user/Server6/log"
    "/home/user/Client1/log"
    "/home/user/Client2/log"
    "/home/user/Client3/log"
)

cp_dirs(){
    for d in ${log_dir[@]}; do
        [ -d $d ] || continue
        files=($(find $d -type f))
        for f in ${files[@]}; do
            [ -d $f ] && continue
            [ "$(file $f | grep ' text')" == "" ] && continue
            dn=$(dirname $f)
            mkdir -p ${TM_DIR}/${dn}
            tail -n 10000 $f > ${TM_DIR}/${f}
        done
    done
    files=($(find /var/log -type f -name 'elplc*' 2> /dev/null))
    [ ${#files[@]} -eq 0 ] && return
    mkdir -p ${TM_DIR}/var/log
    for f in ${files[@]}; do
        [ "$(file $f | grep ' text')" == "" ] && continue
        d=$(basename $f)
        tail -n 10000 $f > ${TM_DIR}/var/log/${d}
    done
}

telemetry(){
    mkdir -p $TM_DIR
    cp_dirs
    echo -e "$(uname -r)\n\n$(cat /etc/os-release)\n" > ${TM_DIR}/sys-info.txt
    journalctl --no-pager > ${TM_DIR}/journalctl.txt
    ps aux > ${TM_DIR}/ps.txt
    netstat -tunlp 2> /dev/null > ${TM_DIR}/netstat.txt
    
    cd $TM_DIR
    tar -czpf ../${UARCH} .
    if [ $? -ne 0 ]; then
        rm -fr ../${UARCH}
#        echo "Failed to create archive"
        cd ../
        rm -fr $TM_DIR
        exit 1
    fi
    
#    echo "Archive created successfully"
    cd ../
    rm -fr $TM_DIR
}

telemetry
echo "${TM_BASE}/${UARCH}"
