from copy import deepcopy
from typing import Any, Dict

from sqlalchemy.orm import Session
from fastapi import APIRouter, Depends, HTTPException

from crud import get_users, get_user, update_password
from schemas import User, UserPasswordUpdate, UserCreate
from dependencies import EWM_Context, CONFIG_PATH, get_current_user, get_db
from fileproc import load_project
from libproc import ElpkWrapper

config_router = APIRouter(prefix="/config", dependencies=[Depends(get_current_user)])
context = EWM_Context()


@config_router.get("")
async def get_config():
    return {key: value for key, value in context.config.items("app")}


@config_router.put("")
async def set_config(settings: Dict[str, str]):
    context.config["app"].update(settings)
    context.save_config()

    context.load_main_context()

    context.log.info("Настройки обновлены")
    return await get_config()


@config_router.get("/users", response_model=list[User])
async def read_users(db: Session = Depends(get_db)):
    users = get_users(db)
    return users


@config_router.post("/users", response_model=User)
async def create_user(user: UserCreate, db: Session = Depends(get_db)):
    db_user = get_user(db, username=user.username)
    if db_user:
        raise HTTPException(status_code=400, detail="Username already registered")
    return create_user(db=db, user=user)


@config_router.get("/users/{username}", response_model=User)
async def read_user(username: str, db: Session = Depends(get_db)):
    db_user = get_user(db, username=username)
    if db_user is None:
        raise HTTPException(status_code=404, detail="User not found")
    return db_user


@config_router.put("/password", response_model=User)
async def change_password(
    password_update: UserPasswordUpdate,
    current_user: User = Depends(get_current_user),
    db: Session = Depends(get_db),
):
    user = update_password(db, current_user, password_update)  # type: ignore
    if user is None:
        raise HTTPException(status_code=400, detail="Incorrect password")
    return user
