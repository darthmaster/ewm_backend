import os
import fcntl
import struct
import socket
import shutil
import logging

from configparser import ConfigParser, NoSectionError
from copy import deepcopy
from threading import Lock

from classes import DataRW, Project
from database import get_db
from fastapi import Depends, HTTPException, status, Query, WebSocket
from fastapi.security import OAuth2PasswordBearer

from jose import JWTError, jwt
from sqlalchemy.orm import Session
from crud import get_user
from schemas import TokenData, Section
from fileproc import load_project, read_xml, save_project
from libproc import ElpkWrapper
from remote import (
    check_health,
    get_remote_serial,
    get_remote_role,
    download_file,
    upload_file,
    manage_remote_service,
)


CONFIG_PATH = "ewm_backend.ini"
CONFIG_DEFAULT = {
    "app": {
        # "projects_dir": "/home/user/elna_projects",
        # "active_project": "project",
        "use_legacy_arrays": "True",
        # "nettalk_port": "7219",
        "serial_number_path": "/opt/serial_number",
        "elpknetdir": "/opt/Elna/network",
        "iousbdir": "/opt/Elna/io.usb",
        "serverdat": "ServerTCP.dat",
        "bsconfig": "bsconfig.xml",
        "log_file": "/home/user/ewm_backend.log",
        "iface_lan1": "enp4s0",
        "iface_lan2": "enp1s0",
        "api_prefix": "/api",
    },
    "controller": {
        "section_role": "local",
        "section_number": 1,
        "main_section_serial_number": "",
        "main_section_lan1": "",
        "main_section_lan2": "",
        "section_1_serial_number": "",
        "section_1_lan1": "",
        "section_1_lan2": "",
        "section_2_serial_number": "",
        "section_2_lan1": "",
        "section_2_lan2": "",
        "section_3_serial_number": "",
        "section_3_lan1": "",
        "section_3_lan2": "",
        "section_4_serial_number": "",
        "section_4_lan1": "",
        "section_4_lan2": "",
        "section_5_serial_number": "",
        "section_5_lan1": "",
        "section_5_lan2": "",
        "section_6_serial_number": "",
        "section_6_lan1": "",
        "section_6_lan2": "",
        "section_7_serial_number": "",
        "section_7_lan1": "",
        "section_7_lan2": "",
    },
    # "bs_commands": {
    #     "test": "echo test cmd output",
    #     "top": "top -bn 1",
    #     "ram": "free -hm",
    #     "net": "netstat -tulnp",
    #     "ps": "ps -eo pid,user,stat,pmem,rss,cpu,comm --sort pcpu",
    #     "temp": "sensors",
    #     "journal": "journalctl --since today",
    # },
}


def get_file_text(path) -> str:
    try:
        with open(path, encoding="utf-8") as file:
            text = file.read()
            return text.strip()
    except FileNotFoundError:
        return "файл не найден"


class SingletonMeta(type):
    _instances = {}
    _lock = Lock()

    def __call__(cls, *args, **kwargs):
        with cls._lock:
            if cls not in cls._instances:
                instance = super().__call__(*args, **kwargs)
                cls._instances[cls] = instance
        return cls._instances[cls]


class EWM_Context(metaclass=SingletonMeta):
    def save_config(self):
        with open(CONFIG_PATH, "w") as config_file:
            self.config.write(config_file)

    def init_logger(self, logfile: str = "ewm_backend.log"):
        strfmt = "[%(levelname)s][%(filename)s %(funcName)s(%(lineno)d)] > %(message)s"  # [%(asctime)s]
        datefmt = "%Y-%m-%d %H:%M:%S"
        log_formatter = logging.Formatter(fmt=strfmt, datefmt=datefmt)

        journal_handler = logging.StreamHandler()
        journal_handler.setLevel(logging.DEBUG)
        journal_handler.setFormatter(log_formatter)

        file_handler = logging.FileHandler(logfile)
        file_handler.setLevel(logging.DEBUG)
        file_handler.setFormatter(log_formatter)

        self.log = logging.getLogger()
        self.log.setLevel(logging.DEBUG)
        self.log.addHandler(file_handler)
        self.log.addHandler(journal_handler)
        self.log.debug("Логгер запущен")

    def load_main_context(self):
        self.init_logger(self.config.get("app", "log_file"))
        self.xml_name = self.config.get("app", "bsconfig")
        self.dat_name = self.config.get("app", "serverdat")
        self.path_iousb = self.config.get("app", "iousbdir")
        self.path_elpknet = self.config.get("app", "elpknetdir")
        self.iface_lan1 = self.config.get("app", "iface_lan1")
        self.iface_lan2 = self.config.get("app", "iface_lan2")
        self.api_prefix = self.config.get("app", "api_prefix")
        self.device_serial = get_file_text(self.config.get("app", "serial_number_path"))
        self.log.info(f"Серийный номер: {self.device_serial}")
        legacy_mode = bool(self.config.get("app", "use_legacy_arrays"))
        self.elpk = ElpkWrapper(legacy=legacy_mode)
        self.log.info(
            f"Библиотека libelpk4.64.so подключена [USE_LEGACY_ARRAYS={legacy_mode}]"
        )

        _socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        try:
            self.ip_lan1 = socket.inet_ntoa(
                fcntl.ioctl(
                    _socket.fileno(),
                    0x8915,  # SIOCGIFADDR
                    struct.pack("256s", bytes(self.iface_lan1[:15], "utf-8")),
                )[20:24]
            )
        except OSError as e:
            if e.errno == 19:
                self.log.warning(f"Интерфейс {self.iface_lan1} не найден")
            else:
                self.log.error(e.strerror)
            self.ip_lan1 = ""
        self.log.info(f"LAN1 IP: {self.ip_lan1}")
        try:
            self.ip_lan2 = socket.inet_ntoa(
                fcntl.ioctl(
                    _socket.fileno(),
                    0x8915,  # SIOCGIFADDR
                    struct.pack("256s", bytes(self.iface_lan2[:15], "utf-8")),
                )[20:24]
            )
        except OSError as e:
            if e.errno == 19:
                self.log.warning(f"Интерфейс {self.iface_lan2} не найден")
            else:
                self.log.error(e.strerror)
            self.ip_lan2 = ""
        self.log.info(f"LAN2 IP: {self.ip_lan2}")

    def load_controller_context(self):
        self.section_role = self.config.get("controller", "section_role")
        self.section_number = self.config.getint("controller", "section_number")

        self.main_section = Section()
        self.main_section.serial_number = self.config.get(
            "controller", "main_section_serial_number"
        )
        self.main_section.lan1 = self.config.get("controller", "main_section_lan1")
        self.main_section.lan2 = self.config.get("controller", "main_section_lan2")

        self.controller_sections = []
        for i in range(1, 8):
            section = Section()
            section.lan1 = self.config.get("controller", f"section_{i}_lan1")
            section.lan2 = self.config.get("controller", f"section_{i}_lan2")
            section.serial_number = self.config.get(
                "controller", f"section_{i}_serial_number"
            )
            self.controller_sections.append(section)

        section_folder = ".controller_project/1"
        os.makedirs(section_folder, exist_ok=True)
        shutil.copy2(
            f"{self.path_iousb}/{self.xml_name}",
            f"{section_folder}/{self.xml_name}",
        )
        if self.section_role == "master":
            for i, section in enumerate(self.controller_sections):
                if section != Section():
                    downloaded = False
                    for host in (section.lan1, section.lan2):
                        if not downloaded and host:
                            base_url = f"https://{host}:8000/api/utils"
                            if check_health(base_url):
                                section_folder = f".controller_project/{i+2}"
                                downloaded = download_file(
                                    base_url,
                                    f"{self.path_iousb}/{self.xml_name}",
                                    section_folder,
                                )
        self.controller_project = load_project(".controller_project")
        self.controller_project.name = ".controller_project"
        self.controller_project.stations[0].ip1 = self.ip_lan1
        save_project(self.controller_project)

    def __init__(self):
        self.config = ConfigParser()
        self.config.read_dict(CONFIG_DEFAULT)
        if not os.path.exists(CONFIG_PATH):
            self.save_config()
        else:
            self.config.read(CONFIG_PATH)

        self.load_main_context()
        self.load_controller_context()

        _, self.working_bsconfig = read_xml(
            str(self.section_number), xmlfile=f"{self.path_iousb}/{self.xml_name}"
        )

        self.cards = {}


app_state = EWM_Context()
oauth2_scheme = OAuth2PasswordBearer(tokenUrl=f"{app_state.api_prefix}/token")
SECRET_KEY = "elna_secret_key:846f8e301e0aae7e1fd1a3cb1b9dab13"
ALGORITHM = "HS256"


async def get_current_user(
    token: str = Depends(oauth2_scheme), db: Session = Depends(get_db)
):
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )
    try:
        payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
        username: str = payload.get("sub")  # type: ignore
        if username is None:
            raise credentials_exception
        token_data = TokenData(username=username)
    except JWTError:
        raise credentials_exception
    user = get_user(db, username=token_data.username)  # type: ignore
    if user is None:
        raise credentials_exception
    return user


async def get_current_user_ws(
    websocket: WebSocket, token: str = Query(...), db: Session = Depends(get_db)
):
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
    )
    try:
        payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
        username: str = payload.get("sub")  # type: ignore
        if username is None:
            raise credentials_exception
        token_data = TokenData(username=username)
    except JWTError:
        await websocket.close(code=status.WS_1008_POLICY_VIOLATION)
        raise credentials_exception
    user = get_user(db, username=token_data.username)  # type: ignore
    if user is None:
        await websocket.close(code=status.WS_1008_POLICY_VIOLATION)
        raise credentials_exception
    return user
