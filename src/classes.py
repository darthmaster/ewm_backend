import json
from dataclasses import dataclass, field
from datetime import datetime
from enum import Enum
from functools import total_ordering
from types import MappingProxyType
from typing import Any, Dict, List, Optional, Tuple, Union

from libproc import ElpkWrapper


class WorkingMode(Enum):
    Reading = 1
    Writing = 2


class ArrayType(Enum):
    DIN = 1
    DOUT = 2
    AIN = 3
    AOUT = 4
    CIRC = 5
    RelSt = 6
    AOIN = 7
    ERR = 8
    SD = 9
    AIW = 10
    SCSC = 11


class OPCQ(Enum):
    NO_QUALITY = 0x00  # Ноль, нет качества, недостоверно
    CONFIG_FAIL = 0x04  # Ошибка конфигурации
    NOT_CONNECTED = 0x08  # Не подключен
    DEVICE_FAILURE = 0x0C  # Ошибка устройства
    SENSOR_FAILURE = 0x10  # Ошибка датчика
    LAST_VALID = 0x14  # Используется последнее достоверное значение
    COMM_FAILURE = 0x18  # Нет связи
    OUT_OF_SERVICE = 0x1C  # Не обслуживается
    INIT_RUN = 0x20  # Выполняется инициализация
    SUCCESS = 0xC0  # Все хорошо, OK


class ModuleState(Enum):
    ERRM_ZERO = 0  # Ноль, не должно быть
    NO_LOCAL_MODULE = 1  # Нет модуля в локальном проекте
    NO_REMOTE_MODULE = 2  # Нет модуля в удаленном проекте
    BAN_EXCHANGE_LOCAL = 3  # Запрет обмена в локальном проекте
    BAN_EXCHANGE_REMOTE = 4  # Запрет обмена в удаленном проекте
    STOPED_LOCAL_THREAD = 5  # Остановлен локальный поток
    STOPED_REMOTE_THREAD = 6  # Остановлен удаленный поток
    LOCAL_THREAD_HUNG = 7  # Завис локальный поток
    REMOTE_THREAD_HUNG = 8  # Завис удаленный поток
    NOT_CONNECTED = 9  # Не подключен
    LOST = 10  # Нет связи
    WRONG_MODULE_TYPE = 11  # Другой тип модуля в проекте
    ZERO_DATAIN = 12  # Нулевые входные данные
    INIT_RUN = 13  # Инициализация
    PORT_NOT_OPENED = 14  # Порт не открыт
    NETWORK_PROG_ERROR = 15  # Ошибка сетевой программы
    INVALID_PROG_VERSION = 16  # Странная версия прошивки модуля
    SUCCESS = 20  # Все хорошо, OK


# Module Channel Display Params
MCDP = MappingProxyType(
    {
        "IA-4k42-M": MappingProxyType({"AIN": 4, "DOUT": 1}),
        "IA-8k42": MappingProxyType({"AIN": 8}),
        "ID-16k24": MappingProxyType({"DIN": 16}),
        "ID-8k24-M": MappingProxyType({"DIN": 8}),
        "IF-3k": MappingProxyType({"AIN": 4, "DIN": 4, "AOUT": 1, "DOUT": 1}),
        "OA-4k42-M": MappingProxyType({"AOUT": 4, "AOIN": 4}),
        "OD-16k24": MappingProxyType({"DOUT": 16, "CIRC": 16, "RelSt": 16}),
        "OD-5k-M": MappingProxyType({"DOUT": 5}),
        "RS-485-4k": MappingProxyType({}),
        "Пусто": MappingProxyType({}),
    }
)


class Module:
    def __init__(
        self,
        str_type="Пусто",
        int_type=0,
        occupied_indices={"ERR": 1},
        params: Dict[str, str] = dict(),
    ):
        self.str_type: str = str_type
        self.int_type: int = int_type
        self.occupied_indices: Dict[str, int] = occupied_indices
        self.initial_indices: Dict[str, int] = dict()
        self.arrays_info: Dict[str, Tuple[int, int]] = dict()
        self.data: Dict[str, List[Any]] = dict()
        self.params: Dict[str, str] = params

    def __repr__(self) -> str:
        return self.str_type

    @classmethod
    def from_code(cls, code: int) -> "Module":
        module_info = {
            2002: ("IA-4k42-M", {"AIN": 4, "DOUT": 1, "ERR": 1}, {}),
            2800: ("IA-8k42", {"AIN": 8, "ERR": 1}, {}),
            6002: ("ID-16k24", {"DIN": 2, "ERR": 1}, {"breakage": "", "power": ""}),
            4002: ("ID-8k24-M", {"DIN": 1, "ERR": 1}, {"breakage": "", "power": ""}),
            1903: (
                "IF-3k",
                {"AIN": 4, "DIN": 4, "AOUT": 1, "DOUT": 1, "ERR": 1},
                {
                    "func": "",
                    "guard": "",
                    "break": "",
                    "lost": "",
                    "timeout": "",
                    "dac": "",
                },
            ),
            3002: ("OA-4k42-M", {"AOUT": 4, "AOIN": 4, "ERR": 1}, {}),
            1600: (
                "OD-16k24",
                {"DOUT": 1, "DOIN": 2, "ERR": 1},
                {"breakage": "", "shortcut": ""},
            ),
            1502: ("OD-5k-M", {"DOUT": 1, "ERR": 1}, {}),
            20: ("RS-485-4k", {"ERR": 1}, {}),
            0: ("Пусто", {"ERR": 1}, {}),
        }
        if code in module_info:
            name, occupied_indices, params = module_info[code]
            return cls(
                str_type=name,
                int_type=code,
                occupied_indices=occupied_indices,
                params=params,
            )
        else:
            return cls()

    @classmethod
    def from_modname(cls, modname: str) -> "Module":
        module_info = {
            "IA-4k42-M": (2002, {"AIN": 4, "DOUT": 1, "ERR": 1}, {}),
            "IA-8k42": (2800, {"AIN": 8, "ERR": 1}, {}),
            "ID-16k24": (6002, {"DIN": 2, "ERR": 1}, {"breakage": "", "power": ""}),
            "ID-8k24-M": (4002, {"DIN": 1, "ERR": 1}, {"breakage": "", "power": ""}),
            "IF-3k": (
                1903,
                {"AIN": 4, "DIN": 4, "AOUT": 1, "DOUT": 1, "ERR": 1},
                {
                    "func": "",
                    "guard": "",
                    "break": "",
                    "lost": "",
                    "timeout": "",
                    "dac": "",
                },
            ),
            "OA-4k42-M": (3002, {"AOUT": 4, "AOIN": 4, "ERR": 1}, {}),
            "OD-16k24": (
                1600,
                {"DOUT": 1, "DOIN": 2, "ERR": 1},
                {"breakage": "", "shortcut": ""},
            ),
            "OD-5k-M": (1502, {"DOUT": 1, "ERR": 1}, {}),
            "RS-485-4k": (20, {"ERR": 1}, {}),
            "Пусто": (0, {"ERR": 1}, {}),
        }
        if modname in module_info:
            int_type, occupied_indices, params = module_info[modname]
            return cls(
                str_type=modname,
                int_type=int_type,
                occupied_indices=occupied_indices,
                params=params,
            )
        else:
            return cls()

    def get_type_tag_dict(self):
        tag_dict = {
            "name": self.str_type,
            **{k.lower(): str(v) for k, v in self.initial_indices.items()},
        }
        if self.str_type in ("ID-8k24-M", "ID-16k24"):
            power_channel = self.params.get("power", "")
            # if power_channel:
            tag_dict["power"] = power_channel
        return tag_dict

    def read(
        self,
        elpk: ElpkWrapper,
        array: Optional[str] = None,
        index: Optional[int] = None,
    ):
        if array:
            if index:
                true_index = self.arrays_info[array][0] + index
                ch_data = elpk.read_channel(array, true_index)
                self.data[array][index] = {
                    "flag": ch_data.flag,
                    "d_value": ch_data.d_value,
                    "a_value": ch_data.a_value,
                }
            else:
                values = []
                for i in range(self.arrays_info[array][1]):
                    ch_data = elpk.read_channel(array, self.arrays_info[array][0] + i)
                    values.append(
                        {
                            "flag": ch_data.flag,
                            "d_value": ch_data.d_value,
                            "a_value": ch_data.a_value,
                        }
                    )
                self.data.update({array: values})
        else:
            for arr, (start_id, id_count) in self.arrays_info.items():
                values = []
                for i in range(id_count):
                    ch_data = elpk.read_channel(arr, start_id + i)
                    values.append(
                        {
                            "flag": ch_data.flag,
                            "d_value": ch_data.d_value,
                            "a_value": ch_data.a_value,
                        }
                    )
                self.data.update({arr: values})
        return self.data

    def write(
        self,
        elpk: ElpkWrapper,
        array: str,
        index: int,
        value: Union[int, float],
        flag: int,
    ):
        write_data = elpk.ChannelData()
        write_data.flag = flag
        if array in ("AIN", "AOUT", "AOIN"):
            write_data.a_value = value
        else:
            write_data.d_value = value
        elpk.write_channel_by_id(
            ArrayType[array].value, self.initial_indices[array] + index, write_data
        )
        self.data[array][index] = write_data
        return self.data

    def to_json(self):
        return json.dumps(
            self, default=lambda o: o.__dict__, ensure_ascii=False, indent=2
        ).encode()


@total_ordering
@dataclass
class BS:
    modules: List[Module] = field(
        default_factory=lambda: [Module.from_modname("Пусто") for _ in range(15)]
    )
    extreme_indices: Dict[str, int] = field(default_factory=dict)
    num: int = 1
    life: int = 0
    cmd: Optional[int] = None
    gpin: Optional[int] = None
    gpout: Optional[int] = None
    param: Optional[int] = None
    power: Optional[int] = 0
    role: str = "Сервер"
    ip1: str = ""
    ip2: str = ""

    cmd_check: bool = False
    gpin_check: bool = True
    gpout_check: bool = True
    param_check: bool = True
    power_check: bool = False
    invert_power_channel: bool = False
    altnum_check: bool = True

    table_rows: List[tuple] = field(default_factory=list)

    def __post_init__(self):
        self.calculate_indices()

    def __repr__(self):
        return f"<БС-{self.num} modules: {self.modules}>"

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            fields_validity = []
            for attr in self.__dict__:
                current = getattr(self, attr)
                loaded = getattr(other, attr)
                is_equals = current == loaded
                fields_validity.append(is_equals)
            return all(fields_validity)
        return False

    def __lt__(self, other):
        return self.num < other.num

    def initialize_extreme_indices(self, extreme_indices):
        if extreme_indices:
            self.extreme_indices = {
                **extreme_indices,
                "ERR": extreme_indices["ERR"] + 1,
            }
        else:
            self.extreme_indices = {
                "AIN": 0,
                "DIN": 0,
                "AOUT": 0,
                "DOUT": 0,
                "AOIN": 0,
                "DOIN": 0,
                "ERR": 1,
                "SD": 16,
                "AIW": 16,
            }

    def is_special_case(self, module, arr_name):
        return (
            self.altnum_check and module.str_type == "OD-16k24" and arr_name == "DOIN"
        )

    def update_module_indices(self, module: Module, arr_name, ch_count):
        if self.is_special_case(module, arr_name):
            init_index = (self.extreme_indices["DOUT"] - 1) * 2
            module.initial_indices[arr_name] = init_index
            self.extreme_indices[arr_name] = init_index + 2
        else:
            module.initial_indices[arr_name] = self.extreme_indices[arr_name]
            self.extreme_indices[arr_name] += ch_count

    def update_array_info(self, module: Module, arr_name, ch_count, arr_inf):
        if arr_name == "DOIN":
            arr_inf.update(
                {
                    "CIRC": (module.initial_indices[arr_name], ch_count),
                    "RelSt": (module.initial_indices[arr_name], ch_count),
                }
            )
        else:
            arr_inf.update({arr_name: (module.initial_indices[arr_name], ch_count)})

    def process_module(self, module: Module):
        arr_inf = {}
        for arr_name, ch_count in module.occupied_indices.items():
            self.update_module_indices(module, arr_name, ch_count)
            self.update_array_info(module, arr_name, ch_count, arr_inf)
        module.arrays_info = arr_inf

    def update_service_indices(self):
        self.life = self.extreme_indices["DIN"]
        self.extreme_indices["DIN"] += 2

        if self.cmd_check:
            self.cmd = self.extreme_indices["DOUT"]
            self.extreme_indices["DOUT"] += 1

        if self.gpin_check:
            self.gpin = self.extreme_indices["DIN"]
            self.extreme_indices["DIN"] += 1

        if self.gpout_check:
            self.gpout = self.extreme_indices["DOUT"]
            self.extreme_indices["DOUT"] += 1

        if self.param_check:
            self.param = self.extreme_indices["AIN"]
            self.extreme_indices["AIN"] += 8

    def calculate_indices(self, extreme_indices=None):
        self.initialize_extreme_indices(extreme_indices)
        for module in self.modules:
            self.process_module(module)
        self.update_service_indices()
        self.update_rows()

    def get_rows_for_table(self) -> List[tuple]:
        rows = []
        for iterator_index, module in enumerate(self.modules):
            rows.append(
                (
                    iterator_index + 1,
                    module.str_type,
                    module.initial_indices.get("AIN", ""),
                    module.initial_indices.get("DIN", ""),
                    module.initial_indices.get("AOUT", ""),
                    module.initial_indices.get("DOUT", ""),
                    module.initial_indices.get("AOIN", ""),
                    module.initial_indices.get("DOIN", ""),
                    module.initial_indices.get("ERR", ""),
                )
            )
        return rows

    def update_rows(self):
        self.table_rows = self.get_rows_for_table()

    def get_modnames(self) -> List[str]:
        names = []
        for index, module in enumerate(self.modules):
            spaces = "  " if index < 9 else " "
            names.append(f" {index+1}.{spaces}{module.str_type}")
        return names


@dataclass
class Project:
    name: str = datetime.now().strftime("%d-%m-%Y %H_%M")
    stations: List[Optional[BS]] = field(default_factory=list) # field(default_factory=lambda: [None for _ in range(8)])

    def append_bs(self, num: int) -> bool:
        is_duplicate = False
        for bs in self.stations:
            if bs and bs.num == num:
                is_duplicate = True
                break
        if not is_duplicate:
            bs = BS(num=num)
            bs.modules = [Module.from_modname("Пусто") for _ in range(15)]
            bs.calculate_indices()
            self.stations.append(bs)
            self.stations.sort()
            return True
        return False

    def remove_bs(self, num: int) -> bool:
        for bs in self.stations:
            if bs and bs.num == num:
                self.stations.remove(bs)
                self.stations.sort()
                return True
        return False

    def set_service_state(self, bs_id: int, check_type: str, check_state: bool):
        if not check_state and not check_type.startswith("power"):
            self.stations[bs_id].__setattr__(check_type[:-6], None)
        self.stations[bs_id].__setattr__(check_type, check_state)
        self.stations[bs_id].calculate_indices()

    def set_module(self, bs_id: int, module_id: int, module_name: str):
        self.stations[bs_id].modules[module_id] = Module.from_modname(module_name)
        self.stations[bs_id].calculate_indices()

    def set_module_params(self, bs_id: int, module_id: int, params: Dict[str, str]):
        for key, value in params.items():
            if key == "mod_power":
                key = "power"
            self.stations[bs_id].modules[module_id].params[key] = value.strip()
            # if stripped_value:
            #     self.stations[bs_id].modules[module_id].params[key] = stripped_value
            # else:
            #     self.stations[bs_id].modules[module_id].params.pop(key, None)

    def find_by_num(self, bsnum: int):
        for bs in self.stations:
            if bs and bs.num == bsnum:
                return bs
        return None

    def calc_nums(self):
        for index, bs in enumerate(self.stations):
            if bs:
                bs.num = index + 1

    def to_json(self):
        return json.dumps(
            self, default=lambda o: o.__dict__, ensure_ascii=False, indent=2
        ).encode()

    def recalc_indices(self, summary=False):
        if summary:
            prv_bs = None
            for bs in self.stations:
                if bs:
                    bs.calculate_indices(prv_bs.extreme_indices if prv_bs else None)
                    prv_bs = bs
        else:
            for bs in self.stations:
                if bs:
                    bs.calculate_indices()


@dataclass
class DataRW:
    id: int
    mode: str = "r"
    array: str = "DIN"
    index: int = 0
    value: int = 0
    flag: int = 0

    def to_dict(self) -> Dict[str, Any]:
        return {
            "id": self.id,
            "mode": self.mode,
            "array": self.array,
            "index": self.index,
            "value": self.value,
            "flag": self.flag,
        }

    def __repr__(self) -> str:
        return str(self.to_dict())
