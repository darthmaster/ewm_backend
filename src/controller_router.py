import os
import shutil
import subprocess
from pathlib import Path
from tempfile import mkdtemp
from zipfile import ZipFile

from fastapi import APIRouter, Depends, UploadFile, WebSocket, HTTPException
from fastapi.responses import FileResponse

from fileproc import save_project, load_project
from dependencies import EWM_Context, get_current_user, get_current_user_ws, CONFIG_PATH
from WebSocketChannel import WebSocketChannel
from schemas import (
    User,
    Section,
    SectionRemoveRequest,
    SectionConnectionRequest,
    SectionConnectionResult,
    ControllerStatus,
    SectionStatus,
    SectionConfigStatus,
)
from remote import (
    check_health,
    get_remote_serial,
    get_remote_role,
    download_file,
    upload_file,
    manage_remote_service,
    set_remote_master,
)

controller_router = APIRouter(prefix="/controller")
context = EWM_Context()


def is_files_identical(file1, file2):
    if os.path.exists(file1) and os.path.exists(file2):
        with open(file1, "r") as f1, open(file2, "r") as f2:
            content1 = f1.read()
            content2 = f2.read()

            return content1 == content2

    return False


def get_section_config_status(
    base_url: str, section: Section, current_serial: str, index: int
) -> SectionConfigStatus:
    config_status = SectionConfigStatus(
        serial_number=section.serial_number == current_serial
    )
    bs_num = index + 2
    shutil.rmtree(f".to_compare/{bs_num}", ignore_errors=True)

    dat_downloaded = download_file(
        base_url,
        f"{context.path_elpknet}/Client1/{context.dat_name}",
        f".to_compare/{bs_num}",
    )
    if dat_downloaded:
        config_status.servertcp = is_files_identical(
            f".controller_project/{bs_num}/{context.dat_name}",
            f".to_compare/{bs_num}/{context.dat_name}",
        )

    xml_downloaded = download_file(
        base_url,
        f"{context.path_iousb}/{context.xml_name}",
        f".to_compare/{bs_num}",
    )
    if xml_downloaded:
        config_status.bsconfig = is_files_identical(
            f".controller_project/{bs_num}/{context.xml_name}",
            f".to_compare/{bs_num}/{context.xml_name}",
        )

    return config_status


async def get_section_status(index: int, section: Section) -> SectionStatus:
    section_status = SectionStatus()
    section_status.bs_number = index + 2
    section_status.lan1 = section.lan1
    section_status.lan2 = section.lan2

    url_template = "http://{}:8000/api/utils"
    lan1_url = url_template.format(section.lan1)
    lan2_url = url_template.format(section.lan2)

    section_status.lan1_online = check_health(lan1_url)
    section_status.lan2_online = check_health(lan2_url)
    if index != -1:
        if section_status.lan1_online:
            section_status.serial_number = get_remote_serial(lan1_url)
            section_status.config_status = get_section_config_status(
                lan1_url, section, section_status.serial_number, index
            )
        elif section_status.lan2_online:
            section_status.serial_number = get_remote_serial(lan2_url)
            section_status.config_status = get_section_config_status(
                lan2_url, section, section_status.serial_number, index
            )
    return section_status


@controller_router.get("")
async def get_controller_summary():
    result = ControllerStatus()
    result.bs_number = context.working_bsconfig.num
    result.role = context.section_role
    result.lan1 = context.ip_lan1
    result.lan2 = context.ip_lan2
    result.serial_number = context.device_serial
    if context.section_role == "master":
        result.slaves = []
        for i, section in enumerate(context.controller_sections):
            if section != Section():
                result.slaves.append(await get_section_status(i, section))
    elif context.section_role == "slave":
        result.master = await get_section_status(-1, context.main_section)
    return result.dict()


controller_channel: WebSocketChannel = WebSocketChannel(get_controller_summary, 2)


@controller_router.websocket("")
async def controller_websocket(
    ws: WebSocket, user: User = Depends(get_current_user_ws)
):
    await controller_channel.handle_connection(ws)


async def update_controller_project():
    shutil.rmtree(".controller_project", ignore_errors=True)
    section_folder = ".controller_project/1"
    os.makedirs(section_folder, exist_ok=True)
    shutil.copy2(
        f"{context.path_iousb}/{context.xml_name}",
        f"{section_folder}/{context.xml_name}",
        follow_symlinks=True,
    )

    listproj = lambda: os.listdir(".controller_project")
    context.log.debug(listproj())
    for i, section in enumerate(context.controller_sections):
        if section != Section():
            downloaded = False
            for host in (section.lan1, section.lan2):
                if not downloaded and host:
                    base_url = f"https://{host}:8000/api/utils"
                    if check_health(base_url):
                        section_folder = f".controller_project/{i+2}"
                        downloaded = download_file(
                            base_url,
                            f"{context.path_iousb}/{context.xml_name}",
                            section_folder,
                        )
            context.log.debug(listproj())

    context.log.debug(listproj())
    context.controller_project = load_project(".controller_project")
    context.controller_project.name = ".controller_project"
    context.controller_project.stations[0].ip1 = context.ip_lan1
    save_project(context.controller_project)


async def update_sections_configs():
    for i, section in enumerate(context.controller_sections):
        if section != Section():
            section_folder = f".controller_project/{i+2}"
            shutil.copy2(
                f"{section_folder}/{context.dat_name}",
                f"{context.path_elpknet}/Server{i+1}/{context.dat_name}",
                follow_symlinks=True,
            )
            uploaded = False
            for host in (section.lan1, section.lan2):
                if not uploaded and host:
                    base_url = f"https://{host}:8000/api/utils"
                    if check_health(base_url):
                        upload_file(
                            base_url,
                            f"{section_folder}/{context.dat_name}",
                            f"{context.path_elpknet}/Client1/{context.dat_name}",
                        )
                        upload_file(
                            base_url,
                            f"{section_folder}/{context.xml_name}",
                            f"{context.path_iousb}/{context.xml_name}",
                        )
                        uploaded = True


async def update_services():
    for i, section in enumerate(context.controller_sections):
        service_name = f"elnaserver{i+1}"
        if section != Section():
            cmd = ["systemctl", "is-enabled", service_name]
            is_service_enabled = (
                subprocess.run(cmd, capture_output=True, text=True).stdout
                == "enabled\n"
            )
            if is_service_enabled:
                cmd = ["systemctl", "restart", service_name]
                subprocess.run(cmd, capture_output=True, text=True)
            else:
                cmd = ["systemctl", "enable", "--now", service_name]
                subprocess.run(cmd, capture_output=True, text=True)
            updated = False
            for host in (section.lan1, section.lan2):
                if not updated and host:
                    base_url = f"https://{host}:8000/api/utils"
                    if check_health(base_url):
                        manage_remote_service(base_url, "elnaiousb", "restart")
                        manage_remote_service(base_url, "elnaclient", "enable")
                        manage_remote_service(base_url, "elnaclient", "restart")
                        updated = True
        else:
            cmd = ["systemctl", "disable", "--now", service_name]
            subprocess.run(cmd, capture_output=True, text=True)
            updated = False
            for host in (
                context.controller_sections[i].lan1,
                context.controller_sections[i].lan2,
            ):
                if not updated and host:
                    base_url = f"https://{host}:8000/api/utils"
                    if check_health(base_url):
                        manage_remote_service(base_url, "elnaclient", "disable --now")
                        updated = True


@controller_router.get("/update")
async def update_controller():
    try:
        await update_controller_project()
    except Exception as e:
        context.log.error(e)
        return {"status": False, "details": "Ошибка пересборки проекта"}
    else:
        context.log.info("Проект обновлён")

    try:
        await update_sections_configs()
    except Exception as e:
        context.log.error(e)
        return {
            "status": False,
            "details": "Ошибка распределения обновлённых конфигурационных файлов",
        }
    else:
        context.log.info("Конфигурационные файлы обновлены")

    try:
        await update_services()
    except Exception as e:
        context.log.error(e)
        return {"status": False, "details": "Ошибка перенастройки сервисов"}
    else:
        context.log.info("Сервисы перезапущены")

    return {"status": True, "details": ""}


@controller_router.post("/section")
async def append_section(request: Section, user: User = Depends(get_current_user)):
    result = SectionConnectionResult()

    if context.section_role != "slave":
        section_id = None
        for i, section in enumerate(context.controller_sections):
            if section == Section():
                if section_id == None:
                    section_id = i
            elif section.serial_number == request.serial_number:
                result.status = False
                result.details = "Секция уже в списке ведомых"
                return result

        if section_id == None:
            result.status = False
            result.details = "Достигнут максимум ведомых секций"
            return result

        section_folder = f".controller_project/{section_id+2}"

        configured = False
        result.online = []
        for host in (request.lan1, request.lan2):
            base_url = f"https://{host}:8000/api/utils"

            result.online.append(check_health(base_url))
            if not result.online[-1]:
                message = f"Хост {host} не доступен; "
                context.log.warning(message)
                result.details += message
                continue

            if not result.status:
                section_serial = get_remote_serial(base_url)
                serial_match = section_serial == request.serial_number
                result.serial_match = serial_match

                section_role = get_remote_role(base_url)
                is_local_role = section_role not in ("slave", "master")

                if not is_local_role:
                    result.status = False
                    result.details = "Целевая секция должна быть в одиночном режиме"
                    break

                if not serial_match:
                    result.status = False
                    result.details = "Неверный серийный номер"
                    break

                section = Section()
                section.lan1 = request.lan1
                section.lan2 = request.lan2
                section.serial_number = request.serial_number

                context.controller_sections[section_id] = section
                context.config.set(
                    "controller", f"section_{section_id+1}_lan1", section.lan1
                )
                context.config.set(
                    "controller", f"section_{section_id+1}_lan2", section.lan2
                )
                context.config.set(
                    "controller",
                    f"section_{section_id+1}_serial_number",
                    section.serial_number,
                )

                master = Section()
                master.lan1 = context.ip_lan1
                master.lan2 = context.ip_lan2
                master.serial_number = context.device_serial
                set_remote_master(base_url, master)

                if context.section_role != "master":
                    context.section_role = "master"
                    context.config.set("controller", "section_role", "master")

                context.save_config()
                context.log.info(
                    f"Секция {section.serial_number} добавлена в конфигурацию"
                )

                update_result = await update_controller()
                result.status = update_result["status"]
                if not result.status:
                    result.details = update_result["details"]
                else:
                    result.details = "Секция успешно подключена"

    else:
        result.status = False
        result.details = "Текущая секция уже является ведомой и не может быть ведущей для других секций"

    return result


@controller_router.delete("/section")
async def remove_last_section():
    result = False
    sections_count = len(context.controller_sections)
    for i in range(sections_count):
        section_index = sections_count - 1 - i
        section = context.controller_sections[section_index]
        if section != Section():
            cmd = ["systemctl", "disable", "--now", f"elnaserver{section_index+1}"]
            subprocess.run(cmd, capture_output=True, text=True)
            disabled = False
            for host in (section.lan1, section.lan2):
                if not disabled and host:
                    base_url = f"https://{host}:8000/api/utils"
                    if check_health(base_url):
                        set_remote_master(base_url, Section())
                        manage_remote_service(base_url, "elnaclient", "disable --now")
                        disabled = True

            context.controller_sections[section_index] = Section()
            context.config.set("controller", f"section_{section_index+1}_lan1", "")
            context.config.set("controller", f"section_{section_index+1}_lan2", "")
            context.config.set(
                "controller", f"section_{section_index+1}_serial_number", ""
            )
            result = True
            break

    no_sections = True
    for section in context.controller_sections:
        if section != Section():
            no_sections = False
    if no_sections:
        context.section_role = "local"
        context.config.set("controller", "section_role", "local")

    context.save_config()

    return result


@controller_router.delete("")
async def remove_host():
    if context.section_role == "slave":
        cmd = ["systemctl", "disable", "--now", "elnaclient"]
        subprocess.run(cmd, capture_output=True, text=True)
        context.main_section = Section()
        context.section_role = "local"
        context.config.set("controller", "section_role", "local")
        context.config.set("controller", "main_section_lan1", "")
        context.config.set("controller", "main_section_lan2", "")
        context.config.set("controller", "main_section_serial_number", "")
        context.save_config()
        return True
    else:
        return False


@controller_router.get("/export")
async def export_project_from_editor():
    TMP_DIR = mkdtemp()
    save_project(context.controller_project, TMP_DIR)
    ZIP = f"{context.controller_project.name}.zip"
    with ZipFile(ZIP, "w") as zip:
        for entity in Path(f"{TMP_DIR}/{context.controller_project.name}").iterdir():
            if entity.is_dir() and entity.name.isdecimal():
                TMP_XML = f"{entity.as_posix()}/{context.xml_name}"
                TMP_DAT = f"{entity.as_posix()}/{context.dat_name}"
                ZIP_XML = f"{entity.name}/{context.xml_name}"
                ZIP_DAT = f"{entity.name}/{context.dat_name}"
                try:
                    zip.write(TMP_XML, ZIP_XML)
                    zip.write(TMP_DAT, ZIP_DAT)
                except Exception as e:
                    context.log.info(e)
            elif entity.name == "summary.xml":
                zip.write(entity.as_posix(), "summary.xml")
    return FileResponse(
        ZIP, headers={"Content-Disposition": f'attachment; filename="{ZIP}"'}
    )
