import os
import time
import random
import subprocess
from typing import Optional
from threading import Thread, Event

from fastapi import APIRouter, Depends, UploadFile, File, Form, HTTPException
from fastapi.responses import JSONResponse
from starlette.responses import Response

from dependencies import EWM_Context, get_current_user, CONFIG_PATH
from schemas import User, ServiceRequest, Section, FilePath, SoftwareUpgradeRequest

utils_router = APIRouter(prefix="/utils")  # , dependencies=[Depends(get_current_user)])
context = EWM_Context()
randfloat = lambda: random.uniform(1, 24000)
randushort = lambda: random.randint(1, 65535)
service_data = context.elpk.ServiceData()
channel_data = context.elpk.ChannelData()
stop_event = Event()
thread = None
is_running = False

# SERIAL_FILE = "/etc/serial_number"
exec_cmd = lambda cmd: subprocess.run(
    cmd,
    stdout=subprocess.PIPE,
    stderr=subprocess.STDOUT,
    text=True,
).stdout


def random_write_channels():
    for array_name, array_id in TypeIO.items():
        if array_name == "SCSC":
            for i in range(8):
                for j in range(2):
                    service_data.gpio[j] = randushort()
                    service_data.life[j] = randushort()
                    service_data.f_gpio[j] = 0xC0  # randushort()
                    service_data.f_life[j] = 0xC0  # randushort()
                for k in range(8):
                    service_data.ain[k] = randfloat()
                    service_data.f_ain[k] = 0xC0  # randushort()
                context.elpk.write_service(i, service_data)
            continue
        for i in range(1024):
            if array_name in ("AIN", "AOUT", "AOIN"):
                channel_data.a_value = randfloat()
            else:
                channel_data.d_value = randushort()
            channel_data.flag = 0xC0  # randushort()
            context.elpk.write_channel_by_id(array_id, i, channel_data)


async def imitation_cycle():
    global is_running
    is_running = True
    while not stop_event.is_set():
        random_write_channels()
        time.sleep(0.02)
    is_running = False


@utils_router.post("/imit")
async def toggle_simulation():
    global thread, stop_event, is_running

    if is_running:
        stop_event.set()
        if thread:
            thread.join()
        return {"status": "stopped"}
    else:
        stop_event.clear()
        thread = Thread(target=imitation_cycle, daemon=True)
        thread.start()
        return {"status": "started"}


@utils_router.get("/imit")
async def get_status():
    return {"status": "running" if is_running else "stopped"}


@utils_router.get("/health")
async def health_check():
    return {"status": "ok"}


@utils_router.get("/serial")
async def get_serial():
    # if not os.path.exists(SERIAL_FILE):
    #     raise HTTPException(status_code=404, detail="Serial file not found")
    # with open(SERIAL_FILE, "r") as f:
    #     serial = f.read().strip()
    return {"serial": context.device_serial}


@utils_router.get("/role")
async def get_role():
    return {"role": context.section_role}


@utils_router.put("/master")
async def set_master_section(master: Section):
    if master == Section():
        context.section_role = "local"
        context.config.set("controller", "section_role", "local")
    else:
        context.section_role = "slave"
        context.config.set("controller", "section_role", "slave")
    context.main_section = master
    context.config.set("controller", "main_section_lan1", master.lan1)
    context.config.set("controller", "main_section_lan2", master.lan2)
    context.config.set("controller", "main_section_serial_number", master.serial_number)
    context.save_config()
    return {"details": "done"}


@utils_router.post("/file")
async def download_file(file_path: FilePath):
    path = file_path.path

    if not os.path.exists(path):
        raise HTTPException(status_code=404, detail="File not found")

    if not os.access(path, os.R_OK):
        raise HTTPException(status_code=403, detail="Access denied")

    with open(path, "rb") as f:
        content = f.read()

    return Response(content, media_type="application/octet-stream")


@utils_router.post("/file/upload")
async def upload_file(
    file: UploadFile = File(...), remote_path: Optional[str] = Form(None)
):
    if remote_path is None:
        remote_path = file.filename
    dir_path = os.path.dirname(remote_path)
    if dir_path and not os.path.exists(dir_path):
        os.makedirs(dir_path, exist_ok=True)
    with open(remote_path, "wb") as f:
        f.write(file.file.read())
    return {"detail": f"File uploaded to {remote_path}"}


@utils_router.post("/service/manage")
async def manage_service(req: ServiceRequest):
    cmd = ["systemctl", *req.action.split(), req.service_name]
    result = subprocess.run(cmd, capture_output=True, text=True)
    if result.returncode != 0:
        raise HTTPException(
            status_code=500, detail=f"Error executing command: {result.stderr}"
        )
    return {"detail": f"Service {req.service_name} {req.action}ed successfully"}


@utils_router.post("/upgrade")
async def upgrade_software(req: SoftwareUpgradeRequest):
    result = ""
    cmds = []

    if req.full or req.system:
        cmds.append(["apt-get", "update"])
        cmds.append(["apt-get", "-y", "dist-upgrade"])

    if req.full or req.frontend:
        cmds.append(
            [
                "git",
                "config",
                "--global",
                "--add",
                "safe.directory",
                "/opt/Elna/ewm-app",
            ]
        )
        cmds.append(["git", "-C", "/opt/Elna/ewm-app", "pull"])
        cmds.append(["yarn", "--cwd", "/opt/Elna/ewm-app", "install"])
        cmds.append(["yarn", "--cwd", "/opt/Elna/ewm-app", "build"])
        cmds.append(["cp", "-r", "/opt/Elna/ewm-app/dist", "/opt/Elna/ewm_front"])
        cmds.append(["systemctl", "restart", "nginx"])

    if req.full or req.backend:
        cmds.append(["git", "-C", "/opt/Elna/ewm_back", "pull"])
        cmds.append(["poetry", "install", "--directory", "/opt/Elna/ewm_back"])
        cmds.append(["systemctl", "restart", "ewm_backend"])

    for output in map(exec_cmd, cmds):
        result += output

    return result
